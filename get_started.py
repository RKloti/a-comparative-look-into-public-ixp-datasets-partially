#!/usr/bin/env python

import sys, os, json, itertools
sys.path.insert (0, "code/peeringdb")
sys.path.insert (0, "code/analysis")
import analysis, mappings


### We need to unpack the data archive first
PDB_ARCHIVE_DIR = os.path.join ("datasets", "peeringdb")
PDB_DATA_DIR = os.path.join (PDB_ARCHIVE_DIR, "data")
if not os.path.isdir (PDB_DATA_DIR):
    print "Untarring PeeringDB data"
    assert 0 == os.system ("cd %s ; tar xf peeringdb-dump.tar.xz" % PDB_ARCHIVE_DIR), "Unpacking PeeringDB data failed"

# Load node data. PeeringDB is special ...
with open('datasets/eu_ix/ixp_nodes.json') as f:
    VE = json.load(f)
VE = {n:v.encode('utf-8') for (n,v) in VE.iteritems()}

with open('datasets/pch/ixp_nodes.json') as f:
    VPCH = json.load(f)
VPCH = {n:v.encode('utf-8') for (n,v) in VPCH.iteritems()}

(IXPs,Participants,Facilities) = analysis.get_last_data(PDB_DATA_DIR)
VP = analysis.get_node_list(IXPs)

# Merge siblings
def merge_siblings (d):
    siblings = mappings.get_ixp_node_set_reductions(d)
    return {siblings.get(k,k):v for (k,v) in d.iteritems()}

VE = merge_siblings(VE)
VP = merge_siblings(VP)
VPCH = merge_siblings(VPCH)

# Compute mappings
# Here we intentionally throw away the ambiguous mappings, even
# if this means that the reverse mapping may not be identical
def compute_mapping (d1, d2):
    mapping = mappings.get_ixp_node_set_mapping(d1, d2)
    unambiguous, ambiguous = mappings.get_ixp_unambiguous_mapping(mapping)
    return unambiguous

datasets = [ "VP", "VE", "VPCH" ]

all_mappings = {}
for d1, d2 in itertools.combinations (datasets, 2):
    data1 = globals()[d1]
    data2 = globals()[d2]
    all_mappings[(d1,d2)] = compute_mapping (data1, data2)
    all_mappings[(d2,d1)] = compute_mapping (data2, data1)

for d1, d2 in itertools.combinations (datasets, 2):
    try:
        map1 = all_mappings[(d1, d2)]
        map2 = all_mappings[(d2, d1)]
    except KeyError:
        print "Couldn't find mappings for", d1, d2
    
    map2_reversed = { v: k for (k, v) in map2.iteritems() }
    if map1 != map2_reversed:
        print "  Mapping and reverse mapping not equivalent for", d1, d2
        print "  Inconsistencies stem from unresolved ambiguities. Feel free to improve the heuristics!"
        keys1 = set(map1)
        keys2 = set(map2_reversed)
        print "  Keys only in %s:" % d1, keys1 - keys2
        print "  Keys only in %s:" % d2, keys2 - keys1
        print "  List of inconsistent mappings:", [ (key, map1[key], map2_reversed[key]) for key in keys1 & keys2
                                if map1[key] != map2_reversed[key]]

print "Check if computed mapping is identical to provided mapping:"

eu_ix_pch = json.load(open('./mapping/eu-ix_pch.json','r'))
print "  Euro-IX and PCH: " + "OK" if eu_ix_pch == all_mappings[("VE","VPCH")] else "Not OK"

eu_ix_pdb = json.load(open('./mapping/eu-ix_peeringdb.json','r'))
print "  Euro-IX and PeeringDB: " + "OK" if eu_ix_pdb == all_mappings[("VE","VP")] else "Not OK"

pdb_pch = json.load(open('./mapping/peeringdb_pch.json','r'))
print "  PeeringDB and PCH: " + "OK" if pdb_pch == all_mappings[("VP","VPCH")] else "Not OK"
 
