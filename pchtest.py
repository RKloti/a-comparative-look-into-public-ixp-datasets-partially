#!/usr/bin/env python

import sys, os, json, itertools
import pch
sys.path.insert (0, "code/peeringdb")
sys.path.insert (0, "code/analysis")
import analysis, mappings

with open('datasets/pch/ixp_nodes.json') as f:
    VPCH = json.load(f)

VPCH = {n:v.encode('utf-8') for (n,v) in VPCH.iteritems()}

import pch
pch_ixp_dict = pch.read_data()

## There seem to be encoding problems with the json files, in particular they
## seem to _not_ be unicode.  The 'encode("utf-8")' (mostly) produces
## the same encoding, and names are identical except for excess
## whitespace and some HTML characters in VPCH. If I omit this step, I
## get plenty of exceptions from mappings.py. The resulting mappings are identical
## except for 'SIX Ko&scaron;ice [Ko&scaron;ice]' ('SIX Ko\xc2\x9aice [Ko\xc2\x9aice]' in VPCH2)
VPCH2 = { ixp.id: ("%s [%s]" % (ixp.metadata['Short Name'], ixp.metadata['City'])).encode("utf-8")
          for ixp in pch_ixp_dict.itervalues() }

assert sorted (VPCH.iterkeys()) == sorted(VPCH2.iterkeys()), "VPCH and VPCH2 use differing keys"
print "Differences between VPCH and VPCH2"
diffs = ( (k, VPCH[k], VPCH2[k]) for k in VPCH if VPCH[k] != VPCH2[k] )
for item in sorted(diffs, key=lambda x: int(x[0])):
    print "%5s %-50r %-50r" % item
print

# Merge siblings
def merge_siblings (d):
    siblings = mappings.get_ixp_node_set_reductions(d)
    return {siblings.get(k,k):v for (k,v) in d.iteritems()}

VPCH_merged = merge_siblings(VPCH)
VPCH2_merged = merge_siblings(VPCH2)

# Compute mappings
# Here we intentionally throw away the ambiguous mappings, even
# if this means that the reverse mapping may not be identical
def compute_mapping (d1, d2):
    mapping = mappings.get_ixp_node_set_mapping(d1, d2)
    unambiguous, ambiguous = mappings.get_ixp_unambiguous_mapping(mapping)
    return unambiguous

mapping_pch_pch2 = compute_mapping (VPCH_merged, VPCH2_merged)
mapping_pch2_pch = compute_mapping (VPCH2_merged, VPCH_merged)

identical = mapping_pch_pch2 == pch.invert_dictionary(mapping_pch2_pch)
print "Forward mapping and reverse mapping identical:", identical
print

with open ("datasets/pch/ixp_edges_ixp_asn.json") as f:
    pch_edges = json.load(f)

pch_ixps = pch_ixp_dict.values()

def get_members4 (ixps):
    def strip_AS (asnstr):
        if asnstr.startswith ("AS"):
            return asnstr[2:]
        else:
            return asnstr
    
    members_iter = pch.iter_data_nonempty (ixps, 4, "ASN")
    return ( [ strip_AS (s) for s in members ] for members in members_iter )
    
pch2_edges = dict (itertools.izip ((ixp.id for ixp in pch_ixps), get_members4 (pch_ixps)) )

assert sorted (pch_edges.iterkeys()) == sorted (pch2_edges.iterkeys()), "PCH edges and PCH2 edges differ in keys"
print "Comparing PCH edges with PCH2 edges"
fmt = "%5s %12s %12s %12s %12s    %s"
print fmt % tuple ("ID #PCH_edges #PCH2_edges #only_PCH #only_PCH2 IXP_longname".split())

sum_only_s1 = 0
sum_only_s2 = 0
num_deviations = 0
for k in sorted (pch_edges, key = int):
    s1 = set (pch_edges[k])
    s2 = set (pch2_edges[k])
    if s1 != s2:
        only_s1 = len (s1 - s2)
        only_s2 = len (s2 - s1)
        sum_only_s1 += only_s1
        sum_only_s2 += only_s2
        num_deviations += 1
        
        print fmt % (k, len(s1), len(s2), only_s1, only_s2, pch_ixp_dict[k].longname)
        #, "PCH only:", sorted (s1-s2, key = int), "PCH2 only:", sorted (s2-s1, key = int)

print
print "Total only PCH:", sum_only_s1, "  only PCH2:", sum_only_s2, "  number of differing edge sets:", num_deviations
