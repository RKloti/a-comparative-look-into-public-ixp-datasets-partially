"""
mappings.py - contains assorted functions performing mapping between the
datasets.
"""
import collections
import fractions
import functools
import itertools
import operator
import logging
import re
import urllib
import numpy

def remove_names(names,rm_set):
    """
    Return names with the entries removed if their IDs match the ones
    listed in rm_set.

    Args:
        names: A mapping of IXPs to sets of IXPs.
        rm_set: A set of IXP to remove from the mapping.

    Returns:
        A mapping where all keys in rm_set have been removed, and all of
        the values (which are sets) have had the values in rm_set removed.
    """
    out_names = {}
    for (k1,v) in names.iteritems():
        if k1 not in rm_set:
            out_names[k1] = {(c,k2) for (c,k2) in v if k2 not in rm_set}
    return out_names

def get_ixp_node_set_reductions(ixp_list,log = logging.getLogger()):
    """
    Return a mapping from a node set onto itself, with redundant items
    merged. This function returns a mapping of IXP IDs to new IDs, where
    IXPs which are equivalent but occur as separate entities are merged
    together.

    Args:
        ixp_list: IXP node list. A mapping of IXP code to name (with city).
        log: logging.Logger object to use to log results to.

    Returns:
        A mapping of IXP IDs to IXP IDs, where duplicate items are merged
        together.
    """
    # This is called to apply one round of filtering, with the IXP names being
    # transformed by the keymap() function.
    def get_new_mapping(ixp_list,keymap=lambda x: x):
        """
        Return a mapping resulting from the application of the keymap, and a new
        list of IXPs, with the matched IXPs removed. We use the keymap as well
        as the matching of the location to qualify two entries as identical.
        This appears to be a useful heuristic.
        """
        city_map = {
            'Quito'                     : 'Quito-Guayaquil/Ecuador',
            'Guayaquil'                 : 'Quito-Guayaquil/Ecuador',
            'Enschede'                  : 'Enschede / M\xc3\xbcnster',
            'M\xc3\xbcnster'            : 'Enschede / M\xc3\xbcnster',
            'Los Angeles'               : 'Los Angeles and Seattle',
            'Seattle'                   : 'Los Angeles and Seattle',
            'Atlanta'                   : 'TX/GA',
            'Mar del Plata'             : 'Buenos Aires',
            'Neuqu\xc3\xa9n'            : 'Buenos Aires',
            'Neuquen'                   : 'Buenos Aires',
            'Santa Fe'                  : 'Buenos Aires',
            'La Plata'                  : 'Buenos Aires',
            'Rosario'                   : 'Buenos Aires',
            'Mar del Tuy\xc3\xba'       : 'Buenos Aires',
            'Bah\xc3\xada Blanca'       : 'Buenos Aires',
            'C\xc3\xb3rdoba'            : 'Buenos Aires',
            'Mendoza'                   : 'Buenos Aires',
            'St Petersburg'             : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Novosibirsk/Russia'        : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Novosibirsk'               : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Khabarovsk'                : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Moscow'                    : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Ufa'                       : 'Saint-Petersburg/Russia, '
                                        + 'Moscow/Russia, Ekaterinburg/Russia,'
                                        + '  Om',
            'Johannesburg'              : 'Johannesburg and Cape Town',
            'Cape Town'                 : 'Johannesburg and Cape Town',
            'Moffett Field'             : 'CA/WA',
            'San Jose'                  : 'CA/WA',
            'Los Angeles'               : 'CA/WA',
            'Seattle'                   : 'CA/WA',
            'Los Angeles &amp; Seattle' : 'CA/WA',
            'Austin'                    : 'TX/GA',
            'Dallas'                    : 'TX/GA',
            'Houston'                   : 'TX/GA',
            'San Antonio'               : 'TX/GA',
            'Singapore'                 : 'Hong Kong / Singapore',
            'Hong Kong'                 : 'Hong Kong / Singapore',
            'UK'                        : 'London', # For PacketExchange,
                                                    # not in RL.
            'Europe / US'               : 'London', # Ditto.
        }
        def process_city_name(city):
            """
            Perform pre-processing of city name.
            """
            return city_map.get(city,city).replace('&amp;','and').upper()
        # Create index of IXPs with keys mapped. The mapping is keyed by
        # by the mapped IDs and contains sets of tuples (ID,name) of IXPs.
        ixp_idx = collections.defaultdict(set)
        for (k,set_of_items) in ixp_list.iteritems():
            ixp_idx[keymap(k)] |= set_of_items
        mapping = {}
        matched_ixps = set()
        # Iterate through all of the abbreviated names.
        for (k,matches) in ixp_idx.iteritems():
            # If there is only one match, then there is no point in adding a
            # mapping, as we cannot merge anything.
            if len(matches) > 1:
                matches_by_city = collections.defaultdict(set)
                for (city,c) in matches:
                    matches_by_city[process_city_name(city)].add(c)
                for (city,city_matches) in matches_by_city.iteritems():
                    if len(city_matches) > 1:
                        for c in city_matches:
                            mapping[c] = k
                            matched_ixps.add(c)
        new_ixp_list = remove_names(ixp_list,matched_ixps)
        return (mapping,new_ixp_list)

    # These are used to extract the necessary information from the node name.
    def city(name):
        """
        Extract city name from node name.
        """
        match_obj = re.match('^.+\[([^]]+)\]$',name)
        return match_obj.group(1) if match_obj else 'Unknown'
    def abbrv(name):
        """
        Extract abbreviated name (minus city).
        """
        match_obj = re.match('^(.+)\s\[[^]]+\]$',name)
        if match_obj is None:
            if re.match('^\s*\[',name):
                return 'Unknown'
            else:
                return name
        else:
            return match_obj.group(1)

    # Dictionaries of IXPs keyed by the name (not the node ID).
    ixp_list_mod = collections.defaultdict(set)
    for (c,n) in ixp_list.iteritems():
        ixp_list_mod[abbrv(n)].add((city(n),c))

    # Key (IXP name) mapping functions.
    # Match IXPs exactly.
    # Match IXPs by converting the name to upper case.
    def linx(name):
        """
        """
        match_obj = re.match('(LINX)(?: Route-Server RS\d)?',name)
        return match_obj.group(1) if match_obj else name
    def upper(name):
        """
        Return the name of an IXP with the characters converted to upper case.
        """
        return name.upper()
    # Match IXPs with miscellaneous substitutions.
    def misc_sub(name):
        """
        Return the name of an IXP with certain substituions performed.
        """
        subs = {
            'Pacific\s?Wave(?: - \w+)?'             : 'PacificWave',
            'NAP Africa.*'                          : 'NAPAfrica',
            'NAP(?:\.EC -| AEPROVI) \w+'            : 'NAP_EC',
            'eXpress'                               : 'PacketExchange',
            'MAE-East(?:-ATM)?'                     : 'MAE_East',
            'MAE-(?:LA|West(?:-ATM)?)'              : 'MAE_West',
            'Equinix(?:[- ]LA| Los Angeles IBX)'    : 'EquinixLA',
        }
        for (n,v) in subs.iteritems():
            name = re.subn(n,v,name)[0]
        return name
    # Match IXPs by cutting at the first space or other non-word character.
    def cut(name):
        """
        Return the name of an IXP up to the first space or other non-word
        character.
        """
        match_obj = re.match('^(\w+)',name)
        if match_obj:
            return match_obj.group(1)
        else:
            name
    # Match IXPs by cutting at the second space or other non-word character.
    def cut2(name):
        """
        Return the name of an IXP up to the second space or other non-word
        character.
        """
        match_obj = re.match('^(\w+\s\w+)',name)
        return match_obj.group(1) if match_obj else name
    # Match IXPs by stripping non-word characters.
    def strip(name):
        """
        Return the name of an IXP with all non-word characters removed.
        """
        return re.subn('[^\w]+','',name)[0]
    # Now try combing strip() and cut().
    def strip_cut(name):
        """
        Return the name of an IXP with non-word characters removed and the
        first word selected.
        """
        match_obj = re.match('^([\w-]+)',name)
        if match_obj:
            return strip(match_obj.group(1))
        else:
            return strip(name)

    # This is the list of keymap functions we use for the mapping. They will be
    # applied in the order that they occur here.
    keymaps = [
        misc_sub,
        linx,
        upper,
        strip,
        strip_cut,
        cut2,
        cut,
    ]

    # These IXPs should not be mapped.
    skip_list = {
        'Equinix Atlanta [Atlanta]',
        'Equinix Dallas [Dallas]',
        'Equinix Los Angeles [Los Angeles]',
        'Equinix Seattle [Seattle]',
        'Equinix San Jose [San Jose]',
        'Equinix IBX San Jose [San Jose]',
        'Equinix IBX Dallas [Dallas]',
        'Equinix-Atlanta [Atlanta]',
        'Equinix-Dallas [Dallas]',
        'Equinix-Seattle [Seattle]',
        'Equinix Hong Kong [Hong Kong]',
        'Equinix Singapore [Singapore]',
        'Pihana Hong Kong [Hong Kong]',
        'Pihana Singapore [Singapore]',
        'B-IX [Sofia]', # Balkan Internet Exchange
        'BIX.BG [Sofia]', # Bulgarian Internet Exchange
        'Telx - Atlanta [Atlanta]',
        'Telx - Dallas [Dallas]',
        'Telx - Phoenix [Phoenix]',
        'Telx Atlanta [Atlanta]',
        'Telx Dallas [Dallas]',
        'Telx Phoenix [Phoenix]',
        'Houston MAGIE [Houston]',
        'Houston MAP [Houston]',
        'Nashville City Net [Nashville]',
        'Nashville NAP [Nashville]',
        'PAIX Atlanta [Atlanta]',
        'PAIX Dallas [Dallas]',
        'SA-IX [Adelaide]',
        'SAIX SE [Adelaide]',
        'NAP Per\xc3\xba [Lima]',
        'NAP Lima [Lima]',
        'NAP do Brasil [S\xc3\xa3o Paulo]',
        'NAP Abranet [S\xc3\xa3o Paulo]',
        'MAE-Dallas [Dallas]',
        'MAE-Houston [Houston]',
        'NSPIXP-1 [Tokyo]',
        'NSPIXP-2 [Osaka]',
        'NSPIXP-3 [Osaka]',
        'NSPIXP-6 [Tokyo]',
    }

    # Iterate through all of the functions, call get_new_mapping() with each
    # function as a parameter. The lists of IXPs are replaced in each iteration,
    # while the mapping is stored to a list.
    mappings = {}
    for keymap in keymaps:
        kn = keymap.func_name
        (map_t,ixp_list_mod) = get_new_mapping(ixp_list_mod,keymap=keymap)
        log.info('Heuristic: [{}] - New mappings: [{}]'.format(kn,len(map_t)))
        for (k,v) in sorted(map_t.items(),key=lambda x: ixp_list[x[0]]):
            # Known bad mappings.
            if ixp_list[k] in skip_list:
                log.debug(' - Skip [{}] ([{}]) -> [{}]'.format(k,ixp_list[k],v))
                continue
            log.debug(' - Mapping: [{}] ([{}]) -> [{}]'.format(k,ixp_list[k],v))
            mappings[k] = v
    return mappings

def get_ixp_unambiguous_mapping(mapping):
    """
    Return an unambiguous (one-to-one) mapping and a one-to-many mapping,
    given a one-to-many mapping. All mappings which are unambiguous will be
    returned in the one-to-many mapping. The remainder will be returned in
    the one-to-many mapping.

    Args:
        mapping: A mapping of keys in one dataset to sets of keys from the
            other datset.

    Returns:
        A tuple, containing a dictionary of mappings from one dataset
        to another and a dictionary of mappings of one dataset to a set
        of items in the other.
    """
    # Preferred mappings for reducing one-to-many mapping.
    pref = {
        '204'       : '1723',
    }
    # Ensure that the ordering is consistent.
    lmap_iter = ((k,v,pref.get(k)) for (k,v) in mapping.iteritems())
    lmapping = ((k,{pref,} if pref in v else v) for (k,v,pref) in lmap_iter)
    lmapping = sorted(lmapping,key=operator.itemgetter(1))
    # Always return first mapping.
    oneone = {k:next(iter(v)) for (k,v) in lmapping if len(v) == 1}
    onemany = {k:v for (k,v) in lmapping if len(v) > 1}
    return (oneone,onemany)

def get_ixp_node_set_mapping(ixp_list1,ixp_list2,log = logging.getLogger()):
    """
    Return a mapping from one IXP's node set IDs to another's.
    This function returns a mapping between the IDs of the nodes in the
    first IXP and those in the second. As there is no guarantee that a
    bijective mapping exists, the map is incomplete: it is not surjective
    in either direction. However, for that subset of the nodes of either
    set for which a mapping exists, it is injective.

    Args:
        ixp_list1: First IXP node list. A mapping of IXP code to name
            (with city).
        ixp_list2: Second IXP node list. A mapping of IXP code to name
            (with city).
        log: logging.Logger object to use to log results to.

    Returns:
        A mapping of IXP IDs to sets of IXP IDs, where IXPs in the
        first node list are mapped to those apparently equivalent in the
        second list.
    """
    # Help functions for get_new_mapping()
    def city_match(city1,city2):
        """
        Return True if cities could be the same, otherwise False.
        This function will return True if the city names match, either name is
        contained in the other, or either or both names are not specified - that
        is, are equal to None.
        """
        # Map for city names. This is used to compensate different spellings as
        # well as the use of region and national names instead of cities. Also,
        # some of the IXPs are distributed, and entered under different cities
        # in the databases.
        city_map = {
            'Seattle'                               : 'West Coast',
            'Romania'                               : 'Bucharest',
            'Lyngby'                                : 'Copenhagen',
                                                    # Suburb of Copenhagen
            'Bilbao'                                : 'Bilboa',
            'Lejona/Pa\xc3\xads Vasco'              : 'Bilboa',
                                                    # Suburb of Bilbao
            'Malm\xc3\xb6'                          : 'Malmoe',
            'D\xc3\xbcsseldorf'                     : 'Duesseldorf',
            'Dusseldorf'                            : 'Duesseldorf',
            'Nurnberg'                              : 'Nuremberg',
            'Nuernberg'                             : 'Nuremberg',
            'Luxemburg'                             : 'Luxembourg',
            'Jakarta'                               : 'Indonesia',
            'Warsaw'                                : 'Poland', # Obvious.
            'Warszawa'                              : 'Poland', # In Polish.
            'Eugene'                                : 'Oregon',
                                                    # City in Oregon.
            'Ipswich'                               : 'London',
                                                    # Not really true...
            'The Hague'                             : 'Amsterdam', # Ditto...
            'Manilla'                               : 'Manila',
            'Makati City'                           : 'Manila',
            'Honoloulu'                             : 'Honolulu',
            'Metro Manila'                          : 'Manila',
            'Port au Prince'                        : 'Port-au-Prince',
            'Saint Denis'                           : 'Saint-Denis',
            'Saint-Petersburg, Russia'              : 'St. Petersburg',
            'St.-Petersburg'                        : 'St. Petersburg',
            'St.Petersburg'                         : 'St. Petersburg',
            'St Petersburg'                         : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Novosibirsk'                           : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Novosibirsk/Russia'                    : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Khabarovsk'                            : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Moscow'                                : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Ufa'                                   : 'Saint-Petersburg/Russia,'
                                                    + ' Moscow/Russia,'
                                                    + ' Ekaterinburg/Russia,'
                                                    + '  Om',
            'Neuqu\xc3\xa9n'                        : 'Buenos Aires',
            'La Plata'                              : 'Buenos Aires',
            'Mar del Tuy\xc3\xba'                   : 'Buenos Aires',
            'Bah\xc3\xada Blanca'                   : 'Buenos Aires',
            'Neuquen'                               : 'Buenos Aires',
            'Santa Fe'                              : 'Buenos Aires',
            'Rosario'                               : 'Buenos Aires',
            'C\xc3\xb3rdoba'                        : 'Buenos Aires',
            'Mendoza'                               : 'Buenos Aires',
            'Johannesburg'                          : 'Johannesburg and'
                                                    + ' Cape Town',
            'Cape Town'                             : 'Johannesburg and'
                                                    + ' Cape Town',
            'Austin'                                : 'Texas',
            'Dallas'                                : 'Texas',
            'Houston'                               : 'Texas',
            'San Antonio'                           : 'Texas',
            'Carrollton'                            : 'Texas',
                                                    # Suburb of Dallas.
            'Saint-Etienne'                         : 'Rhone Alpes',
            'Saint Etienne'                         : 'Rhone Alpes',
                                                    # City in Rhone Alpes.
            'Petach Tikva'                          : 'Tel Aviv',
                                                    # City is nearby.
            'Tel-Aviv'                              : 'Tel Aviv',
            'Turin'                                 : 'Torino',
            'Edinburgh'                             : 'Scotland', # Obvious.
            'Brussels'                              : 'Belgium', #Obvious.
            'Strasbourg'                            : 'Alsace',
            'Salt Lake City'                        : 'Utah', # Ditto.
            'Rostov-on-Don'                         : 'Rostov on Don',
            'Hag\xc3\xa5t\xc3\xb1a'                 : 'Guam', # City in Guam.
            'Adelaide/SA'                           : 'Adelaide',
            'Cheyenne'                              : 'WY/MT/CO',
                                                    # City in Wyomig (WY).
            'Columbus'                              : 'Ohio', # City in Ohio.
            'No. VA &amp; NYC'                      : 'Vienna No. VA and NYC '
                                                    + 'and Washington, DC',
            'Vienna'                                : 'Vienna No. VA and NYC '
                                                    + 'and Washington, DC',
                                                    # For both.
            'Ashburn'                               : 'Northern Virginia',
            'North-Virginia'                        : 'Northern Virginia',
                                                    # City in NV.
            'Saint George'                          : 'Southern Utah',
                                                    # Mutatis mutandis.
            'Moffett Field'                         : 'San Jose '
                                                    + 'and Los Angeles',
            'Moffett Field, CA'                     : 'San Jose '
                                                    + 'and Los Angeles',
                                                    # Nearby airport.
            'Mountain View'                         : 'San Jose '
                                                    + 'and Los Angeles',
            'Mountain View, CA'                     : 'San Jose '
                                                    + 'and Los Angeles',
            'Msida'                                 : 'Malta',  # City in Malta.
            'Eschen'                                : 'LI',
                                                    # Eschen is in Liechtenstein
            'Cura\xc3\xa7ao'                        : 'Curacao',
            'Salvador/BA'                           : 'Salvador',
            'S\xc3\x83\xc2\xa3o Paulo'              : 'Sao Paulo',
            'S\xc3\xa3o Paulo'                      : 'Sao Paulo',
            'Cuiab\xc3\xa1'                         : 'Cuiaba',
            'Bel\xc3\xa9m'                          : 'Belem',
            'Vit\xc3\xb3ria'                        : 'Vitoria',
            'S\xc3\xa3o Jos\xc3\xa9 dos Campos'     : 'Sao Jose dos Campos',
            'S\xc3\xa3o Jos\xc3\xa9 do Rio Preto'   : 'Sao Jose do Rio Preto',
            'Florian\xc3\xb3polis'                  : 'Florianopolis',
            'Bras\xc3\xadlia'                       : 'Brasilia',
            'San Juan de lima'                      : 'Lima',
            'Krak\xc3\xb3w'                         : 'Krakow',
            'Ko&scaron;ice'                         : 'Kosice',
            'Troms\xc3\xb8'                         : 'Tromso',
            'Troms\xc3\x83\xc2\xb8'                 : 'Tromso',
            'Lule\xc3\xa5'                          : 'Lulea',
            'Ume\xc3\xa5'                           : 'Umea',
            'Macclesfield'                          : 'Manchester', # Nearby.
            'UK'                                    : 'London Manchester',
            'Europe / US'                           : 'London',
                                                    # Only for PacketExchange.
            'Lefkosia'                              : 'Nicosia',
                                                    # Alternative name.
        }
        def process_city_name(city):
            """
            Perform pre-processing of city name.
            """
            # Map city1 and city2 according to the given mapping. If no mapping
            # exists (the normal case), just use the city name without any
            # mapping.
            city = city_map.get(city,city)
            # Replace ampersand with 'and'.
            # upper() ensures that the capitalisation is consistent.
            return city.replace('&amp;','and').upper()
        # Return true if either city is not defined. This prevents an otherwise
        # correct match from being prevented due to missing location info.
        if not city1 or not city2:
            return True
        else:
            # Do pre-processing.
            city1 = process_city_name(city1)
            city2 = process_city_name(city2)
            # Exact match, plus check to see if either city name is a substring
            # of the other.
            return city1 == city2 or city1 in city2 or city2 in city1

    # This is called to apply one round of filtering, with the IXP names being
    # transformed by the keymap() function.
    def get_new_mapping(ixp1,ixp2,keymap=lambda x: x):
        """
        Return new mapping of IXP1 IDs to IXP2 IDs, as well as the the IXP
        dictionaries with the entries contained in the returned mapping removed.
        Entries have their names transformed by the keymap() function supplied.
        They are considered matching if the transformed names match and the
        cities are either the same or one or both is not available.
        """
        # Create index of IXPs with keys mapped. The mapping is keyed by
        # by the mapped IDs and contains sets of tuples (ID,name) of IXPs.
        ixp1_idx = collections.defaultdict(set)
        for (k,set_of_items) in ixp1.iteritems():
            ixp1_idx[keymap(k)] |= set_of_items
        ixp2_idx = collections.defaultdict(set)
        for (k,set_of_items) in ixp2.iteritems():
            ixp2_idx[keymap(k)] |= set_of_items
        # This contains the set of keys that occur in both data sets. All other
        # keys do not constitute possible mappings.
        match_keys = ixp1_idx.viewkeys() & ixp2_idx.viewkeys()
        kn = keymap.func_name
        log.debug('Heuristic: [{}] - Matches: [{}]'.format(kn,len(match_keys)))
        # For each key with matches, get set of all elements in either dataset
        # which map to the key in question.
        mapping = collections.defaultdict(set)
        (matched_ixps1,matched_ixps2) = (set(),set())
        for k in match_keys:
            match_pairs = itertools.product(ixp1_idx[k],ixp2_idx[k])
            for ((city1,c1),(city2,c2)) in match_pairs:
                if city_match(city1,city2):
                    mapping[c1].add(c2)
                    matched_ixps1.add(c1)
                    matched_ixps2.add(c2)
        # Obtain a mapping of the node IDs of the first IXP dictionary to the
        # node IDs of the second IXP dictionary.
        new_ixp1 = remove_names(ixp1,matched_ixps1)
        new_ixp2 = remove_names(ixp2,matched_ixps2)
        return (mapping,new_ixp1,new_ixp2)

    # These are used to extract the necessary information from the node name.
    def city(name):
        """
        Extract city name from node name.
        """
        match_obj = re.match('^.+\[([^]]+)\]$',name)
        return match_obj.group(1) if match_obj else 'Unknown'
    def abbrv(name):
        """
        Extract abbreviated name (minus city).
        """
        match_obj = re.match('^(.+)\s\[[^]]*\]$',name)
        return match_obj.group(1) if match_obj else 'Unknown'   

    # Dictionaries of IXPs keyed by the name (not the node ID).
    ixps = []
    for ixp_list in (ixp_list1,ixp_list2):
        ixps_by_name = collections.defaultdict(set)
        for (c,n) in ixp_list.iteritems():
            ixps_by_name[abbrv(n)].add((city(n),c))
        ixps.append(ixps_by_name)

    # Key (IXP name) mapping functions.
    # Match IXPs exactly.
    def exact(name):
        """
        Return the name of an IXP unaltered.
        """
        return name
    # Match IXPs by converting the name to upper case.
    def upper(name):
        """
        Return the name of an IXP with the characters converted to upper case.
        """
        return name.upper()
    # Match IXPs by cutting at the first space or other non-word character.
    def cut(name):
        """
        Return the name of an IXP up to the first space or other non-word
        character.
        """
        match_obj = re.match('^(\w+)',name)
        return match_obj.group(1) if match_obj else name
    # Match IXPs by cutting at the second space or other non-word character.
    def cut2(name):
        """
        Return the name of an IXP up to the second space or other non-word
        character.
        """
        match_obj = re.match('^(\w+\s\w+)',name)
        return match_obj.group(1) if match_obj else name
    # Match Any2/CoreSite.
    def csaix(name):
        """
        Return the name of an IXP mapped so that Any2/CoreSite IXPs match.
        Others are ignored.
        """
        subs = {
            'NorthEast'             : 'Reston',
            'Northern California'   : 'San Jose',
        }
        for (n,v) in subs.iteritems():
            name = re.subn(n,v,name)[0]
        match_obj1 = re.match('Any2 \(CoreSite\) ([\w\s]+)',name)
        match_obj2 = re.match('CoreSite - Any2 ([\w\s]+)',name)
        match_obj = match_obj1 if match_obj1 else match_obj2
        return 'Any2-{}'.format(match_obj.group(1)) if match_obj else name
    # Match PTT IXPs.
    def pttix(name):
        """
        Return the name of an IXP mapped so that PTT IXPs match. Others are
        ignored.
        """
        match_obj1 = re.match('PTT-(\w+)',name)
        match_obj2 = re.match('PTT [\w\s]+ \((\w+)\)',name)
        match_obj = match_obj1 if match_obj1 else match_obj2
        return 'PTT-{}'.format(match_obj.group(1)) if match_obj else name
    # Match PTT IXPs II.
    def pttix2(name):
        """
        Return the name of an IXP mapped so that PTT IXPs match. Matches
        additional entries. Others are ignored.
        """
        mappings = {
            'Belem'                     : 'BEL',
            'Caxias do Sul'             : 'CXJ',
            'Manaus'                    : 'MAO',
            'Natal'                     : 'NAT',
            'Sao Jose do Rio Preto'     : 'SJP',
            'Vitoria'                   : 'VIX',
        }
        match_obj = re.match('PTT (.+)',name)
        if match_obj:
            return 'PTT-{}'.format(mappings.get(match_obj.group(1),name))
        else:
            return name
    # Match IX Australia.
    def ixaus(name):
        """
        Return the name of an IXP mapped so that IX Australia IXPs match.
        Others are ignored.
        """
        match_obj1 = re.match('(?:(VIC|QLD|SA|NSW|WA)-?IX(?! SE))',name)
        match_obj2 = re.match('IX Australia (\w+)',name)
        match_obj = match_obj1 if match_obj1 else match_obj2
        return 'IX Austr. {}'.format(match_obj.group(1)) if match_obj else name
    # Match IXPs with miscellaneous substitutions.
    def misc_sub(name):
        """
        Return the name of an IXP with certain substituions performed.
        """
        subs = {
            'HIX(?: Haiti)?'                        : 'AIX',
            'RDC-IX Kinshasa'                       : 'KINIX',
            'CIIXP'                                 : 'CIVIX',
            'FICIX[12]'                             : 'FICIX',
            'FICIX3'                                : 'FICIX - Oulu',
            'IIX(?:_| - Tel Aviv)'                  : 'IIX',
            'InterLAN-IX'                           : 'INTERLAN',
            u'D.+sseldorf'                          : 'Duesseldorf',
            'Balkan-'                               : 'B-',
            'GIX - Accra'                           : 'GIX',
            'GIXA'                                  : 'GIX',
            'BIX - Jakarta'                         : 'BIX',
            'BIXc'                                  : 'BIX',
            'MINAP Milan'                           : 'MiNAP',
            '(?:IXP-(?:ML|MegaLink)|IX-LAPAZ)'      : 'IXP-ML',
            'NPIX - Kathmandu'                      : 'npIX',
            '(?:R-iX|R_iX)'                         : 'R_ix',
            '(?:MIX - Manila|Manila IX)'            : 'MIX',
            'NGIX'                                  : 'BAYANTEL',
            'Terremark(?! -)(?: NAP)?(?: de las)?'  : 'NOTA',
            'Terremark - NAP de las Americas C.*'   : 'NOTA',
            ' Internet Exchange'                    : 'IX',
            'NAP CABASE - .*'                       : 'CABASE',
            'DataIX - .+'                           : 'DATAIX',
            'NAP Africa(?: Cape Town)?'             : 'NAPAfrica',
            'Angola-IXP / ANG-IXP'                  : 'ANG-IX',
            'ASM-IX HK'                             : 'AMS-IX Hong Kong',
            'BLNX/ECIX BER'                         : 'ECIX Berlin',
            'eXchange West'                         : 'ExWest',
            'DRFxchange'                            : 'DRF IX',
            'NAP VSIX IPv4'                         : 'VSIX',
            'VUIX'                                  : 'VIX',
            'KAZ-IX'                                : 'Unknown',
            'Lesotho IX'                            : 'LIXP',
            '(?:Malawi IXP|MIX-BT)'                 : 'MIX',
            'Samara-IX'                             : 'SMR-IX',
            'Chelyabinsk '                          : 'CHEL-',
            'MOZ-?IX'                               : 'MIX',
            'WHKIX'                                 : 'WIXP',
            'NIX1'                                  : 'NIX',
            'CAT-THIX'                              : 'NIX-CAT',
            'Giganet IX'                            : 'GigaNET',
            'NetNod Stockholm'                      : 'Netnod',
            'NAP INCA'                              : 'Unknown',
            'NAP(?:\.Peru| Per\xc3\xba)'            : 'NAP Peru',
            'EUNIC'                                 : 'DN',
            'GigaPix OPorto'                        : 'GIGA-PIX',
            'XP-London IXP'                         : 'XchangePoint',
            'MAE-'                                  : 'MAE - ',
            'CoreSite - Any2(?! Los| North| Cal).*' : 'Coresite - Any 2',
                                                    # Negative look-ahead
                                                    # assertion.
            'CoreSite - Any2 Northern California'   : 'Coresite - Any 2',
            'Any(?:\s)?2 \(CoreSite\).*'            : 'Coresite - Any 2',
                                                    # For EuroIX -> PCH
            'PTTMetro S\xc3\xa3o Paulo'             : 'PTT-SP',
            'NAP CABASE.*'                          : 'CABASE',
            'CABASE-POS'                            : 'CABASE',
            'CoreSite - Any2 (?:Los Angeles|Cal).*' : 'Any2',
            'CoreSite - Any2 NorthEast'             : 'Coresite - Any 2',
            'Pacific\s?Wave.*'                      : 'PacificWave',
            'AMES-MIX'                              : 'NASA-AIX',
            'OpenIXP / NiCE'                        : 'Open IXP',
        }
        for (n,v) in subs.iteritems():
            name = re.subn(n,v,name)[0]
        return name
    # Match IXPs by removing certain prefixes.
    def nopre(name):
        """
        Return the name of an IXP with certain prefixes removed.
        """
        remove = {
            'Equinix ',
            'Terremark - ',
            'GPX ',
            'Angola-IXP / ',
            'OpenIXP / ',
            'MalmIX Malmo / ',
            ' Malta',
            ' NO',
            ' Gothenburg',
            'Netnod - ',
            'M9-IX/',
            'IBX ',
        }
        for v in remove:
            name = name.replace(v,'')
        return name
    # Match IXPs by stripping non-word characters.
    def strip(name):
        """
        Return the name of an IXP with all non-word characters removed.
        """
        return re.subn('[^\w]+','',name)[0]
    # As above, but now trying combining strip with upper
    def strip_upper(name):
        """
        Return the name of an IXP with non-word characters removed and word
        characters converted to upper-case.
        """
        return upper(strip(name))
    # Now try combing strip() and cut().
    def strip_cut(name):
        """
        Return the name of an IXP with non-word characters removed and the
        first word selected.
        """
        match_obj = re.match('^([\w-]+)',name)
        return strip(match_obj.group(1) if match_obj else name)
    # This is the list of keymap functions we use for the mapping. They will be
    # applied in the order that they occur here.
    keymaps = [
        exact,
        upper,
        pttix,
        pttix2,
        csaix,
        ixaus,
        misc_sub,
        nopre,
        cut2,
        cut,
        strip,
        strip_upper,
        strip_cut,
    ]

    # Iterate through all of the functions, call get_new_mapping() with each
    # function as a parameter. The lists of IXPs are replaced in each iteration,
    # while the mapping is stored to a list.
    mappings = collections.defaultdict(set)
    for keymap in keymaps:
        kn = keymap.func_name
        (map_t,ixps[0],ixps[1]) = get_new_mapping(ixps[0],ixps[1],keymap=keymap)
        for (k,v) in map_t.iteritems():
            if len(v) > 0:
                dest_set = ' and '.join([ixp_list2[d] for d in v])
                log.debug(' - {}: {} -> {}'.format(kn,ixp_list1[k],dest_set))
            mappings[k] |= v
        log.info('Heuristic: [{}] - New mappings: [{}]'.format(kn,len(map_t)))
    return mappings

def prune_node_set_mappings(mapping,eu_ix_ixp_extra_info,pdbsnapshot,key):
    """
    Return a new node mapping which contains only entries that can be
    corroborated with further evidence. Currently it cross checks the
    website (company website) of the IXP. It is conservative in choosing
    what entries to eliminate; if one value is missing from either dataset
    then this is not taken into consideration for deletion. Note that the
    function get_ixp_node_set_mapping() already takes location into account.

    Args:
        mapping: The node set mapping to prune.
        eu_ix_ixp_extra_info: Dictionary of extra information, for EuroIX
            dataset.
        pdbsnapshot: Data structure derived from PeeringDB. Note that this
            must be obtained externally.
        key: Which data source (eu_ix or peeringdb) was used for the keys
            of mapping (the other data source was used for the values).

    Returns:
        A node set mapping, i.e., of IXP IDs to IXP IDs, which contains
        only mappings which can be corroborated with another data source.

    Raises:
        ValueError: If key is not either eu_ix or peeringdb.
    """
    if not key in {'eu_ix','peeringdb'}:
        raise ValueError('Value of key must be either eu_ix or peeringdb')
    peeringdb_ixp_extra_info = pdbsnapshot.ixps
    # These top level domains allow direct subdomains to be registered:
    # For instance, ethz.ch, rather than ethz.ac.uk (if it were a British
    # university).
    tlds_with_direct_subdomains = {
        'com','net','org','br','ch','no','bg','pl','ca','cz','it','be','gr',
        'ie','fi','ru','se','fr','de','ro','at','lu','is','rs','pt',
    }
    def get_website_eu_ix(eu_ix_name):
        """
        Return the website field given the name in the EuroIX dataset.
        """
        # This is the contents of the field.
        extra_info = eu_ix_ixp_extra_info.get(eu_ix_name)
        # Extra info is not available for all IXPs.
        if extra_info is None:
            return None
        url = extra_info.get('Website')
        # This is the host component of the URL.
        host = urllib.splithost(urllib.splittype(url)[1])[0]
        # Remove subdomains. We need to check if it is a national TLD or an
        # international one, so we know how many components we need.
        if not host:
            return None
        components = re.split('\.',host)
        slice_idx = -2 if components[-1] in tlds_with_direct_subdomains else -3
        return ('.'.join(components[slice_idx:])).lower()
    def get_website_peeringdb(peeringdb_name):
        """
        Return the company website field given the name in the PeeringDB
        dataset.
        """
        # This is the contents of the field.
        ixp_obj = peeringdb_ixp_extra_info[int(peeringdb_name)]
        url = ixp_obj.common['Company Website']
        # This is the host component of the URL.
        host = urllib.splithost(urllib.splittype(url)[1])[0]
        # Remove subdomains. We need to check if it is a national TLD or an
        # international one, so we know how many components we need.
        if not host:
            return None
        components = re.split('\.',host)
        slice_idx = -2 if components[-1] in tlds_with_direct_subdomains else -3
        return ('.'.join(components[slice_idx:])).lower()

    def is_match_eu_ix(eu_ix_name,peeringdb_name):
        """
        This function processes mappings which are keyed by EuroIX names.
        It simply calls is_match_peeringdb with reversed arguments.
        """
        return is_match_peeringdb(peeringdb_name,eu_ix_name)
    def is_match_peeringdb(peeringdb_name,eu_ix_name):
        """
        This function processes mappings which are keyed by PeeringDB names.
        """
        website_eu_ix = get_website_eu_ix(eu_ix_name)
        website_peeringdb = get_website_peeringdb(peeringdb_name)
        # We need to handle the case where there is no information available
        # from the EuroIX or PeeringDB dataset. Here we choose to view this as a
        # match.
        return not website_eu_ix \
            or not website_peeringdb \
            or website_eu_ix == website_peeringdb
    is_match = {
        'eu_ix'     : is_match_eu_ix,
        'peeringdb' : is_match_peeringdb,
    }[key]
    return {k:v for (k,v) in mapping.iteritems() if is_match(k,v)}

def ixp_node_set_mapping_overlap(mapping,ixp_adjacent_asn1,ixp_adjacent_asn2):
    """
    Return a new node mapping containing the same mapping as the old one,
    in addition to the overlap between the two IXPs in either data set,
    which is defined as the portion of the elements in the smaller set
    which are present in the larger set. Therefore a value of 0.0 means
    that there is no overlap, while a value of 1.0 means that the smaller
    set is a subset of the larger (and that they are identical if the sets
    are of the same size). This can be considered to be a measure of the
    quality of the node identifier mapping.

    Args:
        mapping: The node set mapping to use for generating the overlap
            list.
        ixp_adjacent_asn1: Mapping of IXPs in the keys of mapping to sets
            of member AS numbers.
        ixp_adjacent_asn2: Mapping of IXPs in the values of mapping to sets
            of member AS numbers.

    Returns: A new mapping, where the values are replaced with tuples
        containing the mapped value and a float representing the portion
        of ASes common to the the key and value AS.
    """
    def get_overlap(ixp1,ixp2):
        """
        Return the number of ASNs the IXPs in each data set have in common.
        """
        asn_set1 = ixp_adjacent_asn1[ixp1]
        asn_set2 = ixp_adjacent_asn2[ixp2]
        size_of_smaller_set = min(len(asn_set1),len(asn_set2))
        return len(asn_set1 & asn_set2)/float(size_of_smaller_set)
    return {k:(v,get_overlap(k,v)) for (k,v) in mapping.iteritems()}

def get_ixp_node_set_mapping_from_overlap(sorted_ixp_adjacency_count1,
                                          sorted_ixp_adjacency_count2,
                                          overlap
                                          ):
    """
    Return a pair of dictionaries mapping IXPs from dataset to those in
    an other data set, based on the number of members that they have in
    common. The first dictionary contains all of the mappings where an
    unambiguous match could be found (not necessarily correct). The
    second dictionary contains mappings where there are multiple maximal
    matches, so all are contained in a set.

    Args:
        sorted_ixp_adjacency_count1: A list of tuples of IXP code and number
            of adjacent addresses, sorted by the latter. Only the ordering
            is important, the number of announced addresses is not used.
        sorted_ixp_adjacency_count2: A list of tuples of IXP code and number
            of adjacent addresses, sorted by the latter. Only the ordering
            is important, the number of announced addresses is not used.
        overlap: A matrix of overlap values, where the rows and columns
            are in the same order as the IXPs in sorted_ixp_adjacency_count1
            and sorted_ixp_adjacency_count2, respectively.

    Returns:
        A mapping of IXP codes in sorted_ixp_adjacency_count1 to sets of
        those in sorted_ixp_adjacency_count2 which have maximal overlap.

    Raises:
        ValueError: If sorted_ixp_adjacency_count1 doesn't match the number
            of rows in overlap, or sorted_ixp_adjacency_count2 doesn't match
            the number of columns.
    """
    (dim_x,dim_y) = overlap.shape
    if not dim_x == len(sorted_ixp_adjacency_count1):
        raise ValueError('Number of rows in overlap must match the '
                        'lenth of sorted_ixp_adjacency_count1.')
    if not dim_y == len(sorted_ixp_adjacency_count2):
        raise ValueError('Number of columns in overlap must match the '
                        'lenth of sorted_ixp_adjacency_count2.')
    # The order must be kept here.
    ixp_list1 = zip(*sorted_ixp_adjacency_count1)[0]
    ixp_list2 = zip(*sorted_ixp_adjacency_count2)[0]
    is_one = functools.partial(operator.eq,1.0)
    matches = collections.defaultdict(set)
    for i in xrange(dim_x):
        row = overlap[i]
        ixp1 = ixp_list1[i]
        for k in xrange(dim_y):
            if is_one(row[k]):
                ixp2 = ixp_list2[k]
                matches[ixp1].add(ixp2)
    return matches

def get_ixp_membership_overlap_matrix(sorted_ixp_adjacency_count1,
                                      sorted_ixp_adjacency_count2,
                                      ixp_adjacent_asn1,
                                      ixp_adjacent_asn2
                                      ):
    """
    Return a matrix containing the overlap between AS sets of IXPs for
    two (different) data sets. Note that the overlap is defined here as
    the magnitude of the intersection divided by the magnitude of the
    union (ixp_node_set_mapping_overlap() uses a different definition).
    The values are normalised so that the largest value on a row is equal
    to 1.0.

    Args:
        sorted_ixp_adjacency_count1: A list of tuples of IXP code and number
            of adjacent addresses, sorted by the latter. Only the ordering
            is important, the number of announced addresses is not used.
        sorted_ixp_adjacency_count2: A list of tuples of IXP code and number
            of adjacent addresses, sorted by the latter. Only the ordering
            is important, the number of announced addresses is not used.
        ixp_adjacent_asn1: A mapping of IXPs to sets of AS numbers, from the
            same data source as sorted_ixp_adjacency_count1.
        ixp_adjacent_asn2: A mapping of IXPs to sets of AS numbers, from the
            same data source as sorted_ixp_adjacency_count2.

    Returns:
        A matrix of overlap factors (see above) as floats. The rows are the
        IXPs in sorted_ixp_adjacency_count1, while the columns are the IXPs
        in sorted_ixp_adjacency_count2.
    """
    def f(index1,index2,asn_set1,asn_set2):
        """
        Return indices and overlap for given values.
        """
        union_size = len(asn_set1 | asn_set2)
        overlap = len(asn_set1 & asn_set2)/float(max(1,union_size))
        return ((index1,index2),overlap)
    len1 = len(sorted_ixp_adjacency_count1)
    len2 = len(sorted_ixp_adjacency_count2)
    ixps1 = ((k,sorted_ixp_adjacency_count1[k][0]) for k in xrange(len1))
    ixps2 = ((k,sorted_ixp_adjacency_count2[k][0]) for k in xrange(len2))
    asns1 = ((k,ixp_adjacent_asn1[ixp]) for (k,ixp) in ixps1)
    asns2 = ((k,ixp_adjacent_asn2[ixp]) for (k,ixp) in ixps2)
    res = (f(x,y,s1,s2) for ((x,s1),(y,s2)) in itertools.product(asns1,asns2))
    overlap_matrix = numpy.zeros((len1,len2))
    for ((x,y),overlap) in res:
        overlap_matrix[x,y] = overlap
    normalisation = [max(row) for row in overlap_matrix]
    for (x,y) in itertools.product(xrange(len1),xrange(len2)):
        norm = normalisation[x]
        if norm == 0: continue
        overlap_matrix[x,y] = overlap_matrix[x,y] / norm
    return overlap_matrix

def get_mapped_ixps(ixp_list,ixp_asn_list,ixp_adjacent_asn,mapping):
    """
    Return IXP node and edge lists which mapped according to the given
    mapping. Where no mapping exists, the IXP ID will remain the same.
    The mapping must be unambiguous. Both the ASN-to-IXP and IXP-to-ASN
    lists will be returned. This function may also be used to merge
    redundant IXPs together.

    Args:
        ixp_list: The IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_asn_list: The mapping of AS number to the codes of the IXPs that
            it is a member of. From the same data source as ixp_list.
        ixp_adjacent_asn: A mapping of IXP codes to sets of AS numbers of
            member ASes.  From the same data source as ixp_list.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second.

    Returns:
        A tuple of the IXP node list, the ASN-to-IXP ID mapping and the
        IXP ID-to-ASN mapping.
    """
    new_ixp_list = {mapping.get(k,k):v for (k,v) in ixp_list.iteritems()}
    # Transform the edge list (ASN to IXP ID mapping).
    new_ixp_asn_list = collections.defaultdict(set)
    for (asn,ixp_list) in ixp_asn_list.iteritems():
        new_ixp_asn_list[asn] = {mapping.get(ixp,ixp) for ixp in ixp_list}
    # The same for the IXP ID to ASN mapping.
    new_ixp_adjacent_asn = collections.defaultdict(set)
    for (ixp,asn_list) in ixp_adjacent_asn.iteritems():
        new_ixp_adjacent_asn[mapping.get(ixp,ixp)].update(asn_list)
    return (new_ixp_list,new_ixp_asn_list,new_ixp_adjacent_asn)

def get_operator_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping,op):
    """
    Return node and edge list which are based on a set theoretic operator.
    This function returns new node and edge lists based on the node and
    edges lists which are supplied and the mapping, which can be generated
    from get_ixp_node_set_mapping(). The nodes will have the IDs and names
    of the second IXP node list, when these overlap with the first.
    The edge sets will be calculated according to the given operator.

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.
        op: An operator function. It must take two sets as a parameter
            and return a set.

    Returns:
        A tuple consisting of an IXP list, a ASN-to-IXP mapping and a
        IXP-to-ASN mapping, the membership of which is based on the
        given set-theoretic operator. The IXP names are mapped to
        those the second data source.

    Raises:
        ValueError: If op is not callable.
    """
    if not callable(op):
        raise ValueError('operator must be callable')

    # Map namespace of first dataset into second.
    ds1 = (ixp_list1,ixp_asn_list1,ixp_adjacent_asn1,mapping)
    (ixp_list1,ixp_asn_list1,ixp_adjacent_asn1) = get_mapped_ixps(*ds1)

    # Form the union of the node sets.
    ixps = set(op(ixp_list1.viewkeys(),ixp_list2.viewkeys()))
    def mapped_name(ixp):
        """
        Return the name of the newly mapped node.
        """
        return ixp_list2[ixp] if ixp in ixp_list2 else ixp_list1[ixp]
    ixp_list = {ixp:mapped_name(ixp) for ixp in ixps}

    # All of the IXPs and ASNs that occur in either dataset.
    all_ixps = set(ixp_adjacent_asn1.viewkeys() | ixp_adjacent_asn2.viewkeys())
    all_asns = set(ixp_asn_list1.viewkeys() | ixp_asn_list2.viewkeys())

    # These functions return the ASNs mapped to an IXP and vice versa.
    s = set
    def mapped_asns(ixp):
        """
        Return the set of ASNs mapped to a given IXP code.
        """
        return op(ixp_adjacent_asn1.get(ixp,s()),ixp_adjacent_asn2.get(ixp,s()))
    def mapped_ixps(asn):
        """
        Return the set of IXP codes mapped to a given ASN.
        """
        return op(ixp_asn_list1.get(asn,s()),ixp_asn_list2.get(asn,s()))
    ixp_asn_list = {asn:mapped_ixps(asn) for asn in all_asns}
    ixp_adjacent_asn = {ixp:mapped_asns(ixp) for ixp in all_ixps}
    return (ixp_list,ixp_asn_list,ixp_adjacent_asn)

def get_union_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping):
    """
    Return node and edge list which are the union of two sets of each.
    This function returns new node and edge lists based on the node and
    edges lists which are supplied and the mapping, which can be generated
    from get_ixp_node_set_mapping(). The nodes will have the IDs and names
    of the second IXP node list, when these overlap with the first. The
    edges are merged together such that any AS which is adjacent to the IXP
    in either edge list will be adjacent to it in the output list.
    This is a convenience function which calls get_operator_ixps().

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.

    Returns:
        A tuple consisting of an IXP list, a ASN-to-IXP mapping and a
        IXP-to-ASN mapping, which contain all IXPs and connections which
        are present in either data source. The IXP names are mapped to
        those the second data source.
    """
    return get_operator_ixps(
        ixp_list1,
        ixp_list2,
        ixp_asn_list1,
        ixp_asn_list2,
        ixp_adjacent_asn1,
        ixp_adjacent_asn2,
        mapping,
        operator.or_
    )

def get_intersect_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping):
    """
    Return node and edge lists which are the intersection of two sets of
    each. This function returns new node and edge lists based on the node
    and edge lists which are supplied and the mapping, which can be
    generated from get_ixp_node_set_mapping(). All of the nodes will have
    the IDs and names of the second IXP node list, with any other nodes
    being omitted. For the edges, only IXPs found in both edge lists will
    be considered. The resulting set of edges for the IXP consists only
    of those edge present in both sets. This is a convenience function
    which calls get_operator_ixps().

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.

    Returns:
        A tuple consisting of an IXP list, a ASN-to-IXP mapping and a
        IXP-to-ASN mapping, which contain all IXPs and connections which
        are present in both data sources. The IXP names are mapped to
        those the second data source.
    """
    return get_operator_ixps(
        ixp_list1,
        ixp_list2,
        ixp_asn_list1,
        ixp_asn_list2,
        ixp_adjacent_asn1,
        ixp_adjacent_asn2,
        mapping,
        operator.and_
    )

def get_difference_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping):
    """
    Return node and edge lists which are the difference of two sets of
    each. This function returns new node and edge lists based on the node
    and edge lists which are supplied and the mapping, which can be
    generated from get_ixp_node_set_mapping(). All of the nodes will have
    the IDs and names of the second IXP node list, with any other nodes
    being omitted. For the edges, only IXPs found in the first edge lists
    but not the second will be considered. The resulting set of edges
    for the IXP consists only of those edge present in the first set but
    not the second.
    This is a convenience function which calls get_operator_ixps().

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.

    Returns:
        A tuple consisting of an IXP list, a ASN-to-IXP mapping and a
        IXP-to-ASN mapping, which contain all IXPs and connections which
        are present in the first data source, but not the second.
        The IXP names are mapped to those the second data source.
    """
    return get_operator_ixps(
        ixp_list1,
        ixp_list2,
        ixp_asn_list1,
        ixp_asn_list2,
        ixp_adjacent_asn1,
        ixp_adjacent_asn2,
        mapping,
        operator.sub
    )

def get_symmetric_difference_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping):
    """
    Return node and edge lists which are the symmetric difference of two
    sets of each. This function returns new node and edge lists based on
    the node and edge lists which are supplied and the mapping, which can
    be generated from get_ixp_node_set_mapping(). All of the nodes will
    have the IDs and names of the second IXP node list, with any other nodes
    being omitted. For the edges, only IXPs found in either the first or the
    second edge lists, but not both will be considered. The resulting set
    of edges for the IXP consists only of those edge present in the either
    set but not both. This is a convenience function which calls
    get_operator_ixps().

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.

    Returns:
        A tuple consisting of an IXP list, a ASN-to-IXP mapping and a
        IXP-to-ASN mapping, which contain all IXPs and connections which
        are present in either data source, but not both.
        The IXP names are mapped to those the second data source.
    """
    return get_operator_ixps(
        ixp_list1,
        ixp_list2,
        ixp_asn_list1,
        ixp_asn_list2,
        ixp_adjacent_asn1,
        ixp_adjacent_asn2,
        mapping,
        operator.xor
    )

def get_matching_ixps(ixp_list1,ixp_list2,ixp_asn_list1,ixp_asn_list2,ixp_adjacent_asn1,ixp_adjacent_asn2,mapping):
    """
    Return two lists of IXPs from the two given, where the names are mapped
    and any nodes not present in both lists are eliminated. The purpose of
    this is to allow the datasets to be compared directly.

    Args:
        ixp_list1: The first IXP node list, a mapping of IXP code to name.
            From the first data source.
        ixp_list2: The second IXP node list, a mapping of IXP code to name.
            From the second data source.
        ixp_asn_list1: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list1.
        ixp_asn_list2: The first mapping of AS number to the codes of the
            IXPs that it is a member of. From the same data source as
            ixp_list2.
        ixp_adjacent_asn1: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list1.
        ixp_adjacent_asn2: A mapping of IXP codes to sets of AS numbers of
            member ASes. From the same data source as ixp_list2.
        mapping: A mapping of IXP codes in the first data source to IXP
            codes in the second. The mapping must be a one-to-one mapping.

    Returns:
        A pair of IXP list, of ASN-to-IP mappings and IXP-to-ASN mappings,
        where all of the IXP codes have been mapped according to the
        specified mapping and all nodes removed that do not exist in both
        data sources.
    """
    # Map namespace of first dataset into second.
    ds1 = (ixp_list1,ixp_asn_list1,ixp_adjacent_asn1,mapping)
    (ixp_list1,ixp_asn_list1,ixp_adjacent_asn1) = get_mapped_ixps(*ds1)

    # Get list of IXPs present in both datasets.
    b = set(ixp_list1.viewkeys() & ixp_list2.viewkeys())

    # Filter out IXPs which are not present in both datasets.
    ixps1 = {n:v for (n,v) in ixp_list1.iteritems() if n in b}
    ixps2 = {n:v for (n,v) in ixp_list2.iteritems() if n in b}

    # For the ASN-to-IXP mapping, iterate over all of the ASNs and filter out
    # all of the IXPs which are not present in both sets.
    ixp_asns1 = {asn:(ixps & b) for (asn,ixps) in ixp_asn_list1.iteritems()}
    ixp_asns2 = {asn:(ixps & b) for (asn,ixps) in ixp_asn_list2.iteritems()}

    # For the IXP-to-ASN mapping, only copy the item if its key is in the set
    # of common IXPs.
    ixp_adjacent_asn1 = {ixp:asns
                        for (ixp,asns)
                        in ixp_adjacent_asn1.iteritems()
                        if ixp in b}
    ixp_adjacent_asn2 = {ixp:asns
                        for (ixp,asns)
                        in ixp_adjacent_asn2.iteritems()
                        if ixp in b}

    return (ixps1,ixps2,ixp_asns1,ixp_asns2,ixp_adjacent_asn1,ixp_adjacent_asn2)


