#!/usr/bin/env python

import os, os.path, glob, urlparse, codecs, multiprocessing
from collections import namedtuple, Counter, defaultdict
from pprint import pprint
import BeautifulSoup

class IXP(object):
    def __init__ (self):
        self.id = None
        self.common = dict()
        self.cidrs = set()
        self.facilities = set()
        self.members = set()
        
    def update (self, id_, common, cidrs, facilities, members):
        if self.id is None:
            assert not id_ is None
            self.id = id_
            self.common = common
        else:
            assert self.common == common
            assert self.id == id_

        self.cidrs.update (cidrs)
        self.facilities.update (facilities)
        self.members.update (members)

    def __repr__ (self):
        try:
            return '<IXP {}: "{}">'.format (self.id, self.common["Common Name"])
        except:
            return '<IXP id={}>'.format (self.id)

    def __str__ (self):
        return repr (self)

class Participant(object):
    def __init__ (self):
        self.id = None
        self.ixps = set()
        self.facilities = set()

    def update (self, id_, company, ixps, facilities):
        if self.id is None:
            assert not id_ is None
            self.id = id_
            self.company = company
        else:
            assert self.id == id_

        self.ixps.update (ixps)
        self.facilities.update (facilities)

    def __repr__ (self):
        try:
            return '<Participant {}: "{}">'.format (self.id, self.company["Company Name{}"])
        except:
            return '<Participant {}>'.format (self.id)

    def __str__ (self):
        return repr(self)

class Facility(object):
    def __init__ (self):
        self.id = None
        self.common = None
        self.local_ixps = set()
        self.private_participants = set()

    def update (self, id_, common, local_ixps, private_participants):
        if self.id is None:
            assert not id_ is None
            self.id = id_
            self.common = common
        else:
            assert self.id == id_
            assert self.common == common
        self.local_ixps.update (local_ixps)
        self.private_participants.update (private_participants)

    def __repr__ (self):
        try:
            return '<Facility {}: "{}">'.format (self.id, self.common["Common Name"])
        except:
            return '<Facility {}>'.format (self.id)

    def __str__ (self):
        return repr(self)

def column_texts (n, row):
    res = list()
    for td in row("td"):
        res.append (unicode(td.text))
    return res[:n]

def column_bools (columns, row):
    res = list()
    tds = row("td")
    for column in columns:
        td = tds[column]
        checked = "checked" in dict (td("input")[0].attrs)
        res.append (checked)
    return res

def get_all_bools (souplet):
    res = list()
    inputs = souplet("input")
    for input_ in inputs:
        checked = "checked" in dict (input_.attrs)
        res.append (checked)
    return res

def get_id_from_url (url):
    keyword, url_id = urlparse.urlparse (url).query.split("&")[0].split("=")
    assert keyword == "id"
    return int(url_id)

def get_id_from_row (row, field):
    try:
        link = dict (row("td")[field]("a")[0].attrs)["href"]
        return get_id_from_url (link)
    except Exception, e:
        #print "get_id_from_row :: ", field, e
        #print row
        return None

def row_iter (table, has_header = True):
    header_skipped = False
    for row in table("tr"):
        if "colspan" in dict(row("td")[0].attrs):
            continue
        if has_header and not header_skipped:
            header_skipped = True
            continue
        yield row

IxpFacility = namedtuple ("IxpFacility", "id, name, city, country, partcount")
IxpMember = namedtuple ("IxpMember", "id, name, asn, ip_address, num_ips, policy")
SupportedProtocols = namedtuple ("SupportedProtocols", "ipv4, multicast, ipv6")

def parse_exchange_html (html):
    soup = BeautifulSoup.BeautifulSoup (html, fromEncoding="utf-8")
    common = dict()
    cidrs = set()
    facilities = set()
    members = set()

    common_information = soup("form")[0]
    address_blocks = common_information.findNextSibling()
    local_facilities = address_blocks.findNextSibling()
    peer_list = local_facilities.findNext ("table")
    
    for row in row_iter (common_information, False):
        key, value = column_texts (2, row)
        if key == "Protocols Supported":
            value = SupportedProtocols (*get_all_bools (row))
        common[key] = value

    for row in row_iter (address_blocks):
        cidrs.add (column_texts (3, row)[1])

    for row in row_iter (local_facilities):
        name, city, country, partcount = column_texts (4, row)
        if name.startswith("No records"):
            continue
        facilities.add (IxpFacility(get_id_from_row (row, 0), name, city, country, int(partcount)))

    for row in row_iter (peer_list):
        peer_name, local_asn, ip_address, num_ips, policy = column_texts (5, row)
        if peer_name.startswith("No records"):
            continue
        members.add (IxpMember (get_id_from_row (row, 0), peer_name, int(local_asn), ip_address, int(num_ips), policy))
        
    return common, cidrs, facilities, members

ParticipantIXP = namedtuple ("ParticipantIXP", "id, asn, ip_address, bandwidth")
ParticipantFacility = namedtuple ("ParticipantFacility", "id, asn, city, country, has_sonet, has_ethernet, has_atm")

def safe_int (s):
    return int (s.replace ("&nbsp;", ""))

def parse_participant_html (html):
    soup = BeautifulSoup.BeautifulSoup (html, fromEncoding="utf-8")
    company = dict()
    ixps = set()
    facilities = set()

    tables = soup("table")
    
    company_information = tables[7]
    public_ixps = tables[9]
    private_facilities = tables[10]

    for row in row_iter (company_information, False):
        key, value = column_texts (2, row)
        if key == "Protocols Supported":
            value = SupportedProtocols (*get_all_bools (row))
        company[key] = value

    for row in row_iter (public_ixps):
        ixp_name, asn, ip_address, bandwidth = column_texts (4, row)
        ixps.add (ParticipantIXP(get_id_from_row (row, 0), safe_int(asn), ip_address, safe_int(bandwidth)))

    for row in row_iter (private_facilities):
        facility_name, asn, city, country = column_texts (4, row)
        has_sonet, has_ethernet, has_atm = column_bools ([4,5,6], row)
        facilities.add (ParticipantFacility(get_id_from_row (row, 0), safe_int(asn), city, country,
                                            has_sonet, has_ethernet, has_atm))
    
    return company, ixps, facilities

FacilityIXP = namedtuple ("FacilityIXP", "id, name, long_name, participant_count")
FacilityParticipant = namedtuple ("FacilityParticipant", "id, asn, protocols")

def parse_facility_html (html):
    soup = BeautifulSoup.BeautifulSoup (html, fromEncoding="utf-8")
    common = dict()
    local_ixps = set()
    private_participants = set()
    
    tables = soup("table")

    common_information = tables[7]
    ixps = tables[8]
    participants = tables[9]

    for row in row_iter (common_information, False):
        key, value = column_texts (2, row)
        common[key] = value

    for row in row_iter (ixps):
        ixp_name, ixp_long_name, participant_count = column_texts (3, row)
        if ixp_name.startswith ("No records"):
            continue
        local_ixps.add(FacilityIXP(get_id_from_row (row, 0), ixp_name, ixp_long_name, int(participant_count)))

    for row in row_iter (participants):
        peer_name, asn = column_texts (2, row)
        protocols = SupportedProtocols (*column_bools ([2,3,4], row))
        private_participants.add (FacilityParticipant(get_id_from_row (row, 0), safe_int(asn),
                                                      protocols))

    return common, local_ixps, private_participants

def html_normalize (html):
    return html.replace ("&nbsp;", " ").replace ("\r", " ").replace ("\n", " ")

def file_iterator (pattern):
    print "WORKING ON :: ", pattern
    for f in glob.glob (pattern):
        #print "WORKING ON :: ", f
        id_ = get_id_from_url (f)
        try:
            contents = codecs.open (f, "r", "utf-8").read()
        except:
            contents = codecs.open (f, "r", "latin-1").read()
        if "Database error:" in contents:
            print "Database not connected in", f
        else:
            yield id_, html_normalize (contents)

def _exchange_mp_wrapper (data):
    id_, html = data
    try:
        return id_, parse_exchange_html (html)
    except:
        import traceback
        print  "="*30, "Failure for IXP id", id_
        traceback.print_exc()
        raise

def _part_mp_wrapper (data):
    id_, html = data
    try:
        return id_, parse_participant_html (html)
    except:
        import traceback
        print "="*30, "Failure for participant id", id_
        traceback.print_exc()
        raise

def _facility_mp_wrapper (data):
    id_, html = data
    try:
        return id_, parse_facility_html (html)
    except:
        import traceback
        print  "="*30, "Failure for Facility id", id_
        traceback.print_exc()
        raise

def renice():
    os.nice(10)

def get_data (datapath, pattern, mapfun, data_class):
    p = multiprocessing.Pool(initializer=renice)
    datapool = defaultdict (data_class)
    
    file_iter = file_iterator (os.path.join (datapath, pattern))
    try:
        for data_id, data in p.imap_unordered (mapfun, file_iter):
        #for data_id, data in map (mapfun, file_iter):
            datapool[data_id].update (data_id, *data)
    except Exception, e:
        import traceback
        traceback.print_exc()

    finally:
        p.close()
        p.join ()
    
    return dict(datapool)

PeeringDBSnapshot = namedtuple ("PeeringDBSnapshot", "ixps, participants, facilities")

def read_data (data_path):
    ixps = get_data(data_path, "exchange_view.php*", _exchange_mp_wrapper, IXP)
    participants = get_data(data_path, "participant_view.php*", _part_mp_wrapper, Participant)
    facilities = get_data (data_path, "facility_view.php*", _facility_mp_wrapper, Facility)

    return PeeringDBSnapshot (ixps, participants, facilities)

