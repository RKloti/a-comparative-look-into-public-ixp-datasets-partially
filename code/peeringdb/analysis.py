"""
analysis.py - Analysis of PeeringDB data.
"""
import os
import glob
import time 
import re
import operator
import collections
import itertools
import functools
import multiprocessing
import logging
import numpy
import networkx as nx
import peeringdb_reader

def get_all_data(datadir=os.path.join(os.environ['HOME'],'peeringdb','data')):
    """
    Return data from PeeringDB snapshots. This is a hook into peeringdb_reader.py.
    Return every dataset, in a dictionary keyed by date and (partial) time.

    Args:
        datadir: Directory containing datasets to parse.

    Returns:
        A dictionary of tuples representing PeeringDB snapshots, keyed by
        date. The tuples consist of a list of IXPs, a list of (IXP)
        Participants and a list of Facilities. These lists are actually a
        mapping of an ID to an object.
    """
    def date_from_dname(dname):
        """
        Return the data from the directory name.
        """
        match = re.search('crawl-(\d+)$',dname)
        if not match: return 'unknown'
        ts = time.localtime(int(match.group(1)))
        return '{}-{}-{}:{}'.format(ts.tm_year,ts.tm_mon,ts.tm_mday,ts.tm_hour)
    data = {}
    for path in glob.glob(os.path.join(datadir,'crawl-*')):
        if re.search('tar.\w+$',path): next
        full_path = os.path.join(path, "www.peeringdb.com", "private")
        data[date_from_dname(path)] = peeringdb_reader.read_data(full_path)
    return data

def get_nearest_data(time,datadir=os.path.join(os.environ['HOME'],'peeringdb','data')):
    """
    Return data from PeeringDB snapshots. This is a hook into peeringdb_reader.py.
    Return the data set with the time closest to the given time.

    Args:
        datadir: Directory containing datasets to parse.
        time: Target date/time.

    Returns:
        A tuple representing the latest PeeringDB snapshot. The tuple
        consists of a list of IXPs, a list of (IXP) Participants and a
        list of Facilities. These lists are actually a mapping of an ID
        to an object.
    """
    def mtime(fname):
        """
        Return ctime for specified file.
        """
        return os.stat(fname).st_mtime
    fl = functools.partial(re.search,'\.tar\.')
    ils = itertools.ifilterfalse(fl,glob.iglob(os.path.join(datadir,'crawl-*')))
    path_list = [os.path.join(n,"www.peeringdb.com","private") for n in ils]
    rel_mtimes = {abs(time-mtime(fname)):fname for fname in path_list}
    return peeringdb_reader.read_data(rel_mtimes[min(rel_mtimes.viewkeys())])

def get_last_data(datadir=os.path.join(os.environ['HOME'],'peeringdb','data')):
    """
    Return data from PeeringDB snapshots. This is a hook into peeringdb_reader.py.
    Return latest dataset, based on the mtime.

    Args:
        datadir: Directory containing datasets to parse.

    Returns:
        A tuple representing the latest PeeringDB snapshot. The tuple
        consists of a list of IXPs, a list of (IXP) Participants and a
        list of Facilities. These lists are actually a mapping of an ID
        to an object.
    """
    def mtime(fname):
        """
        Return ctime for specified file.
        """
        return os.stat(fname).st_mtime
    fl = functools.partial(re.search,'\.tar\.')
    ils = itertools.ifilterfalse(fl,glob.iglob(os.path.join(datadir,'crawl-*')))
    path_list = [os.path.join(n,"www.peeringdb.com","private") for n in ils]
    mtimes = {mtime(fname):fname for fname in path_list}
    return peeringdb_reader.read_data(mtimes[max(mtimes.viewkeys())])

def get_node_list(ixps):
    """
    Return list of IXP nodes, in the same format as used by
    cxp_experimentation.

    Args:
        ixps: A mapping of IXP IDs to IXP objects.

    Returns:
        A mapping of IXP IDs to names, with city names in square brackets.
    """
    def format_str(ixp):
        """
        Return the formatted IXP name string.
        """
        name = ixp.common['Common Name'].encode('utf-8')
        city = ixp.common['City'].encode('utf-8')
        return '{} [{}]'.format(name,city)
    return {str(ixp_id):format_str(ixp) for (ixp_id,ixp) in ixps.iteritems()}

def get_edge_list(ixps,participants,log=logging.getLogger()):
    """
    Return list of IXP edges, in the same format as used by
    cxp_experimentation. Two mappings are returned, both the forward
    mapping (ASN -> IXP ID) and the reverse mapping (IXP ID -> ASN).
    This avoids the necessity of reversing the lists manually.

    Args:
        ixps: A mapping of IXP IDs to IXP objects.
        participants: A mapping of Participant IDs to Participant objects.
        log: A Logger object.

    Returns:
        A tuple consisting of a mapping from ASN to a set of IXP IDs and
        a mapping of IXP IDs to a set of ASNs.
    """
    asns_to_ixps = collections.defaultdict(set)
    ixps_to_asns = {str(i.id):set() for i in ixps.itervalues()}
    for participant in participants.itervalues():
        # Note that we do NOT use the "primary ASN" value here,
        # as a participant may appear at different IXPs under different ASNs.
        for participant_ixp in participant.ixps:
            ixp = str(participant_ixp.id)
            asn = str(participant_ixp.asn)
            asns_to_ixps[asn].add(ixp)
            try:
                ixps_to_asns[ixp].add(asn)
            except KeyError as e:
                pid = str(participant.id)
                log.warn('In {}: AS {} => IXP {}'.format(pid,asn,ixp))
    return (asns_to_ixps,ixps_to_asns)

def get_participants_list(participants):
    """
    Return a mapping of AS number to Participant object.

    Args:
        participants: A mapping of Participant IDs to Participant objects.

    Returns:
        A mapping of AS number to Participant object.
    """
    asn_to_part = {}
    for participant in participants.itervalues():
        for participant_ixp in participant.ixps:
            asn = str(participant_ixp.asn)
            asn_to_part[asn] = participant
    return asn_to_part

def dump_ixp_list(ixps,fname):
    """
    Dump list of IXPs to a file, with the same output format as that used
    in cxp_experimentation.

    Args:
        ixps: A mapping of IXP IDs to IXP objects
        fname: Filename to dump IXP list to

    Raises:
        IOError: If the specified filename could not be opened for writing.
    """
    with open(fname,'w') as f:
        for (ixp_id,ixp) in ixps.iteritems():
            attribs = ixp.common
            output_str = '\t'.join([
                str(ixp_id),
                attribs.get('Common Name',str()),
                attribs.get('Long Name',str()),
                attribs.get('City',str()),
                attribs.get('Country',str()),
            ])
            f.write(output_str.encode('utf-8') + '\n')

def dump_ixp_members_list(ixps,fname):
    """
    Dump list of IXP members to a file, with the same output format as that
    used in cxp_experimentation.

    Args:
        ixps: A mapping of IXP IDs to IXP objects
        fname: Filename to dump IXP list to

    Raises:
        IOError: If the specified filename could not be opened for writing.
    """
    with open(fname,'w') as f:
        for (ixp_id,ixp) in ixps.iteritems():
            for member in ixp.members:
                output_str = ' '.join([
                    str(ixp_id),
                    str(member.id),
                    str(member.asn),
                    member.policy,
                ])
                f.write(output_str + '\n')

def dump_to_csv(data_set,fname):
    """
    Dump given data set to specified file name, as a CSV file.
    The type of data to be dumped is either a list of IXPs, a list of
    IXP Participants or a list of Facilities, as returned by get_last_data()
    or get_all_data(). The type will be detected automatically based on the
    first element. The returned CSV will have the following format:
    IXP:
        - ID
        - Common Name
        - City
        - Company Website
        - Continental Region
        - Country
        - Long Name
        - Media Type
        - Policy E-Mail
        - Policy Phone
        - Protocols Supported / IPv4
        - Protocols Supported / IPv6
        - Protocols Supported / Multicasting
        - Technical E-Mail
        - Technical Phone
        - Traffic Statistics Website
    Participant:
        - ID
        - Primary ASN
        - Company Name
        - Also Known As
        - Approx Prefixes
        - Company Website
        - Contract Requirement
        - Date Last Updated
        - General Policy
        - Geographic Scope
        - IRR Record
        - Looking Glass URL
        - Multiple Locations
        - Network Type
        - Notes
        - Peering Policy URL
        - Protocols Supported / IPv4
        - Protocols Supported / IPv6
        - Protocols Supported / Multicasting
        - Ratio Requirement
        - Route Server URL
        - Traffic Levels
        - Traffic Ratios
    Facility:
        - ID
        - Common Name
        - Address 1
        - Address 2
        - CLLI Code
        - City / State / Postal
        - Country Code
        - Facility Management
        - NPA-NXX
        - Website

    Args:
        data_set: Data set to dump. Either a list of IXPs, a list of
            Participants or a list of Facilities. The type will be
            detected automatically from the first element.
        fname: Name of file to dump output to.

    Raises:
        IOError: If specified filename could not be opened for writing.
    """
    def dump_ixp(entry):
        """
        Return array of attributes from entry. The entries are converted
        into strings, if necessary.
        """
        return [
            str(entry.id),
            entry.common['Common Name'],
            entry.common['City'],
            entry.common['Company Website'],
            entry.common['Continental Region'],
            entry.common['Country'],
            entry.common['Long Name'],
            entry.common['Media Type'],
            entry.common['Policy E-Mail'],
            entry.common['Policy Phone'],
            str(entry.common['Protocols Supported'].ipv4),
            str(entry.common['Protocols Supported'].ipv6),
            str(entry.common['Protocols Supported'].multicast),
            entry.common['Technical E-Mail'],
            entry.common['Technical Phone'],
            entry.common['Traffic Statistics Website'],
        ]
    def dump_part(entry):
        """
        Return array of attributes from entry. The entries are converted
        into strings, if necessary.
        """
        return [
            str(entry.id),
            entry.company['Primary ASN'],
            entry.company['Company Name'],
            entry.company['Also Known As'],
            entry.company['Approx Prefixes'],
            entry.company['Company Website'],
            entry.company['Contract Requirement'],
            entry.company['Date Last Updated'],
            entry.company['General Policy'],
            entry.company['Geographic Scope'],
            entry.company['IRR Record'],
            entry.company['Looking Glass URL'],
            entry.company['Multiple Locations'],
            entry.company['Network Type'],
            entry.company['Notes'],
            entry.company['Peering Policy URL'],
            str(entry.company['Protocols Supported'].ipv4),
            str(entry.company['Protocols Supported'].ipv6),
            str(entry.company['Protocols Supported'].multicast),
            entry.company['Ratio Requirement'],
            entry.company['Route Server URL'],
            entry.company['Traffic Levels'],
            entry.company['Traffic Ratios'],
        ]
    def dump_fac(entry):
        """
        Return array of attributes from entry. The entries are converted
        into strings, if necessary.
        """
        return [
            str(entry.id),
            entry.common['Common Name'],
            entry.common['Address 1'],
            entry.common['Address 2'],
            entry.common['CLLI Code'],
            entry.common['City / State / Postal'],
            entry.common['Country Code'],
            entry.common['Facility Management'],
            entry.common['NPA-NXX'],
            entry.common['Website'],
        ]
    dump_f = {
        "<class 'peeringdb_reader.IXP'>"         : dump_ixp,
        "<class 'peeringdb_reader.Participant'>" : dump_part,
        "<class 'peeringdb_reader.Facility'>"    : dump_fac,
    }[str(type(data_set[1]))]
    with open(fname,'w') as f:
        for entry in data_set.itervalues():
            s = '\t'.join(dump_f(entry)) + '\n'
            f.write(s.encode('utf-8'))

def find_duplicate_attributes(data_set):
    """
    Return list of attributes which occur more than once in the specified
    data set (IXPs, participants or facilities). The returned data
    structure specifies the attribute in question, the value of the
    attribute, as well as participants where the value occurs. The purpose
    of the function is to find potential candidates for matching IXPs.

    Args:
        data_set: A list of objects, which may be either IXPs,
            Participants or Facilities, as returned by the functions
            get_all_data() or get_last_data().

    Returns:
        A dictionary of dictionaries of lists. The outer dictionary is
        keyed by an attribute name. The inner dictionary is keyed by
        attribute value and has lists of item IDs as values.
    """
    # attribute_getter returns a dictionary of item-specific attributes.
    # For a Participant, the 'company' attribute contains a dictionary of
    # attributes, for IXPs and facilities the 'common' attribute contains
    # it instead.
    if type(data_set.itervalues().next()) == peeringdb_reader.Participant:
        attribute_getter = operator.attrgetter('company')
        name_getter = operator.itemgetter('Company Name')
    else:
        attribute_getter = operator.attrgetter('common')
        name_getter = operator.itemgetter('Common Name')
    def ident_map(ident):
        """
        Return the name from the specified identifier.
        """
        return name_getter(attribute_getter(data_set[ident]))
    # Essentially a multimap object, but allowing the same element to occur
    # multiple times. We will count the number of occurences of each member
    # in a later stage.
    item_attributes = collections.defaultdict(list)
    # Here, we collect all of the occurences of the values of each attribute,
    # then count them, eliminating single occurences.
    for (ident,item) in data_set.iteritems():
        attributes_dict = attribute_getter(item)
        for (attribute,value) in attributes_dict.iteritems():
            item_attributes[attribute].append(value)
    ctr = collections.Counter
    frq = {attr:ctr(lst) for (attr,lst) in item_attributes.iteritems()}
    # A dictionary of dictionaries of lists - the first index is for the
    # attribute, the second for the value of the attribute. The value is the
    # set of identities of the items to which the attribute belonged.
    duplicates = {attr:collections.defaultdict(list) for attr in frq.iterkeys()}
    for (ident,item) in data_set.iteritems():
        attributes_dict = attribute_getter(item)
        for (attribute,value) in attributes_dict.iteritems():
            if frq[attribute][value] > 1:
                duplicates[attribute][value].append(ident_map(ident))
    # Convert defaultdict back into normal dictionary objects.
    return {attr:dict(attr_dict) for (attr,attr_dict) in duplicates.iteritems()}

def ixps_by_policy(ixps):
    """
    Return list of IXPs by policy.

    Args:
        ixps: A list of IXPs, as returned by get_all_data() or
            get_last_data().

    Returns:
        A dictionary of dictionaries of sets. The outer dictionary is
        keyed by AS number. The inner dictionary is keyed by a policy,
        which is generally either 'Restrictive', 'Selective' or 'Open'.
        The inner dictionary has values consisting of sets of IXP IDs.
    """
    part = set(itertools.chain(*[x.members for x in ixps.itervalues()]))
    ixp_members_by_policy = {p.asn:collections.defaultdict(set) for p in part}
    for (ident,item) in ixps.iteritems():
        ixp_id = item.id
        for member in item.members:
            ixp_members_by_policy[member.asn][member.policy].add(ixp_id)
    return {mem:dict(pol) for (mem,pol) in ixp_members_by_policy.iteritems()}

def filter_by_policy(ixps_by_policy,policy):
    """
    Return a mapping of ASN to IXPs which the ASN is a member of,
    considering only those matching the given policy or have none
    specified.

    Args:
        ixps_by_policy: A data structure mapping IXPs to policy, as
            returned by ixps_by_policy().
        policy: A string specifying a policy, which is either 'Restrictive',
            'Selective' or 'Open' (or an empty string).

    Returns:
        A mapping of AS numbers to sets of IDs of IXP which the AS is a
        member of, assuming that the policy it specifies matches the one
        specified in the policy parameter. 

    Raises:
        ValueError: If the specified policy is not 'Open', 'Selective' or
            'Restrictive'.
    """
    if policy not in {'','Open','Selective','Restrictive'}:
        raise ValueError('policy must be Open, Selective or Restrictive.')
    filtered_by_policies = {}
    for (asn,pol) in ixps_by_policy.iteritems():
        filtered_by_policies[asn] = pol.get('',set()) | pol.get(policy,set())
    return filtered_by_policies

def get_bandwidth_graph(participants,ixps,default=None):
    """
    Return bandwidth graph. The bandwidth graph is a bipartite graph
    consisting of IXP nodes and participant nodes. An edge exists between
    two nodes of different types if the participant is a member of the IXP.
    The edge capacity is set to the specified bandwidth. If there is no
    specified bandwidth, the value default is used instead. If the value
    of default is None, then edges where no bandwidth is known are omitted.
    The graph nodes have the common respectively company property
    dictionaries as attributes.

    Args:
        participants: A data structure mapping participant IDs to
            Participant objects, as returned by the functions
            get_all_data() or get_last_data().
        ixps: A data structure mapping IXP IDs to IXP objects, as returned
            by the functions get_all_data() or get_last_data().
        default: A number that will be used in the event that no bandwidth
            information is available from the data. Converted into a float.

    Returns:
        A bipartite NetworkX graph with one set of nodes representing
        IXPs, another set representing IXP participants, with an edge
        connecting between nodes if the participant is a member of the
        corresponding IXP. The edges are weighted with the bandwidth
        obtained from the source data; if no bandwidth is available
        (or the bandwidth is specified as zero), a default value is used.
        If no default is specified, edges with no bandwidth available will
        not be added to the graph.
    """
    G = nx.Graph()
    # Add nodes for participants.
    participant_names = {}
    for (participant_id,participant) in participants.iteritems():
        participant_name = participant.company['Company Name']
        participant_obj = (participant_name,'Participant')
        participant_names[participant_id] = participant_name
        G.add_node(participant_obj,attr_dict=participant.company)
    # Add nodes for IXPs.
    ixp_names = {}
    for (ixp_id,ixp) in ixps.iteritems():
        ixp_name = ixp.common['Common Name']
        ixp_obj = (ixp_name,'IXP')
        ixp_names[ixp_id] = ixp_name
        G.add_node(ixp_obj,attr_dict=ixp.common)
    for (participant_id,participant) in participants.iteritems():
        for ixp_member in participant.ixps:
            participant_obj = (participant_names[participant.id],'Participant')
            ixp_obj = (ixp_names[ixp_member.id],'IXP')
            bw = ixp_member.bandwidth
            if bw <= 0:
                if default == None:
                    continue
                else:
                    bw = default
            G.add_edge(ixp_obj,participant_obj,{'capacity': float(bw)})
    return G

def get_access_bandwidth(G):
    """
    Return list of nodes with their access bandwidths (i.e. the sum
    of all outgoing edge capacities).

    Args:
        G: A NetworkX graph with edges representing bandwidths, such as
            that returned by get_bandwidth_graph().

    Returns:
        A list of nodes with the name, type and the sum of the bandwidth
        of all incident edges.
    """
    node_bandwidth = []
    get_capacity = operator.itemgetter('capacity')
    for node in G.nodes_iter():
        capacity = sum(get_capacity(edge_d) for edge_d in G[node].itervalues())
        if capacity > 0.0:
            node_bandwidth.append((node[0],node[1],capacity))
    return node_bandwidth

def update_globals_bw_mat(G):
    """
    Utility function for get_bandwidth_matrix().
    Stores the specified values to global variables.

    Args:
        G: A NetworkX graph with edges representing bandwidths, such as
            that returned by get_bandwidth_graph().
    """
    global per_process_G
    per_process_G = G

def process_bw_mat(s,t):
    """
    Utility function for get_bandwidth_matrix()
    Perform per-job action (calculate minimum cut).
    Note that G is stored in a global variable.

    Args:
        s: Name of source node.
        t: Name of target node.

    Returns:
        A float representing the minimum cut between the nodes s and t.
    """
    G = per_process_G
    mincut = nx.min_cut(G,s,t)
    return mincut

def get_bandwidth_matrix(G,querytype='IXP',procs=multiprocessing.cpu_count()):
    """
    Return a matrix of pair-wise minimum cut bandwidths between
    IXPs or participants. Executes in parallel.

    Args:
        G:  A NetworkX graph with edges representing bandwidths, such as
            that returned by get_bandwidth_graph().
        querytype: A string, either 'IXP' or 'Participant', specifying the
            types of nodes to calculate the minimum cut between.
        procs: The number of processes to use. By default, the number of
            processors present on the machine.

    Returns:
        A numpy matrix containing the minimum cut between each pair of nodes
        of the specified type.

    Raises:
        ValueError: If querytype is not either 'IXP' or 'Participant'.
    """
    if querytype not in {'IXP','Participant'}:
        raise ValueError('querytype must be IXP or Participant')

    # List of nodes to query.
    query_list = [name for (name,ntype) in G.nodes_iter() if ntype == querytype]
    pool = multiprocessing.Pool(
                                processes=procs,
                                initializer=update_globals_bw_mat,
                                initargs=(G,)
                               )

    # Set up process pool, insert jobs, wait for jobs to complete.
    jobs = {}
    for (s_idx,t_idx) in itertools.combinations(range(len(query_list)),2):
        (s,t) = ((query_list[s_idx],querytype),(query_list[t_idx],querytype))
        jobs[(s_idx,t_idx)] = pool.apply_async(process_bw_mat,(s,t))
    pool.close()
    pool.join()

    # Compile matrix from results, taking advantage of the symmetry.
    bw_mat = numpy.zeros((len(query_list),len(query_list)))
    for ((x,y),job) in jobs.iteritems():
        capacity = job.get()
        bw_mat[x,y] = capacity
        bw_mat[y,x] = capacity
    return bw_mat
