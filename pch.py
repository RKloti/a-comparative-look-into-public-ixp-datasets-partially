#!/usr/bin/env python

import sys, os, json, itertools, glob, collections, urllib2, operator, multiprocessing

import bs4

PCH_DATA_DIR = os.path.join ("datasets", "pch")
PCH_DUMP_DIR = os.path.join (PCH_DATA_DIR, "dump", "applications", "ixpdir")
PCH_DATA_ARCHIVE = "pch-dump.tar.xz"

if not os.path.isdir (PCH_DUMP_DIR):
    print "Untarring PCH dump"
    assert 0 == os.system ("cd %s ; tar xf %s" % (PCH_DATA_DIR, PCH_DATA_ARCHIVE)), "Unpacking PCH data failed"

PchIxp = collections.namedtuple ("PchIxp", "id, longname, metadata, members4, members6")

def extract_columns (data, columns):
    getter = operator.itemgetter (*columns)
    return [ getter (row) for row in data ]

def get_column (name, h, d):
    try:
        column = h.index (name)
        return extract_columns (d, [column])
    except:
        return None

def get_columns (names, h, d):
    columns = [ h.index (name) for name in names ]
    return extract_columns (d, columns)

def is_ipv4_address (s):
    octets = s.split(".")
    return len(octets) == 4 and all ((octet.isdigit() and 0 <= int(octet) <= 255) for octet in octets)

def is_ipv6_address (s):
    def is_hex (word):
        word = word.lower()
        return word != "" and all (c in "0123456789abcdef" for c in word)
    
    parts = s.split("::")
    if len (parts) > 2:
        return False
    
    for part in parts:
        if part == "":
            continue
        words = part.split(":")
        for word in words:
            if len(word) > 4 or not is_hex(word):
                return False
    
    if len (parts) == 1 and len(words) != 8:
        return False
    
    return True

def estimate_version (IPs):
    if len(IPs) == 0:
        return None
    elif all (is_ipv4_address(ip) for ip in IPs[:10]):
        return 4
    elif all (is_ipv6_address (ip) for ip in IPs[:10]):
        return 6
    else:
        print IPs
        raise ValueError ("Cannot find unique IP version")

def normalize_asns (asns):
    if asns is None:
        return None
    asns = [ asn for asn in asns if not asn in (u'\xa0', '') ]
    assert all ((s.startswith ("AS") and s[2:].isdigit()) for s in asns), "ASN column contains non-ASNs " + repr (asns)
    return asns

def extract_memberships (soup):
    tables = soup ("table")
    members4 = []
    header4 = None
    members6 = []
    header6 = None
    for table in tables:
        header, data, footer = parse_table (table)

        IPs = get_column ("IP", header, data)
        if not IPs is None:
            version = estimate_version (IPs)
            
            if version == 4:
                assert header4 is None or header == header4
                header4 = header
                members4.extend(data)
            elif version == 6:
                assert header6 is None or header == header6
                header6 = header
                members6.extend(data)
            #else:
            #    print "Empty frame"
        else:
            if not header is None and "IP" in header:
                print "Error when matching IP column for", get_shortname (soup), header

    ### These assertions always hold: To find the right column we need to have a header
    ### in the first place
    #assert (not header4 is None) or (len(members4) == 0), "v4 Dataframe without header"
    #assert (not header6 is None) or (len(members6) == 0), "v6 Dataframe without header"
    return (header4, members4), (header6, members6)

def td_value (td):
    text = td.text.strip()
    if text == '':
        if td("img"):
            return td.find("img").attrs["src"]
        else:
            # if text is empty and there is no image, there might
            # still be a HTML tag which we want to preserve
            return td.renderContents().decode("utf-8").strip()
    else:
        return text

def parse_table (table):
    rows = table("tr")
    header = rows[0]("th")
    if len (header) > 0:
        header = [ th.text for th in header ]
    else:
        header = None

    datastart = 1 if header else 0

    data = [ [ td_value(td) for td in tr("td") ] for tr in rows[datastart:] ]

    ## Sometimes, there are more columns in the header than the
    ## data. In these cases, the first header column is empty or
    ## "&nbsp;". So lets get rid of it. 
    if not header is None and len (data) > 0 and len(header) == len(data[0]) + 1:
        assert header[0] in ("", u"\xa0"), "data/header width mismatch"
        del header[0]

    # Sometimes, the last data row actually is a footer. Report it
    # separately.
    if len(data) > 2 and len(data[-1]) < len(data[-2]):
        footer = data.pop()
    else:
        footer = None

    assert len(set(len(r) for r in data)) <= 1, "Data contains rows of variable length"
    assert header is None or len(data) == 0 or len (data[0]) == len(header), "Header and data differ in width"

    return header, data, footer

def get_shortname (soup):
    return soup.find ("table")("tr")[0]("td")[1].text

def get_longname (soup):
    return soup.find ("h1").text

def get_metadata (soup):
    h, d, f = parse_table (soup.find ("table"))
    return dict (d)

def invert_dictionary (d):
    ret = dict ( (v, k) for k, v in d.iteritems())
    assert len(d) == len(ret), "Multiple values are identical in the original dictionary"
    return ret

pch_ixp_nodes = json.load (open("datasets/pch/ixp_nodes.json"))
pch_ixp_edges = json.load (open("datasets/pch/ixp_edges_asn_ixp.json"))
pch_id_to_url = json.load (open("../papers/cxp_experimentation/data/pch/ixp_nodes_urls.json"))
pch_url_to_id = invert_dictionary(pch_id_to_url)

def url_from_filename (name):
    name = urllib2.unquote(name)
    index = name.find ("/applications/ixpdir/detail.php?id=")
    assert index >= 0, "Don't know how to handle this name"
    return name[index:]

def read_detail (detail):
    html = open (detail).read()
    soup = bs4.BeautifulSoup (html)
    members4, members6 = extract_memberships (soup)
    url = url_from_filename (detail)
    pch_id = pch_url_to_id[url]
    pch_edges = set (pch_ixp_edges.get (pch_id, []))

    return PchIxp (pch_id, get_longname(soup), get_metadata(soup), members4, members6)

def renice():
    os.nice(10)

def read_data ():
    details = glob.glob (os.path.join (PCH_DUMP_DIR, "detail.php*"))

    # ixps = map (read_detail, details)
    pool = multiprocessing.Pool(initializer=renice)
    ixps = pool.imap_unordered (read_detail, details)
    pool.close()
    
    return dict ( (ixp.id, ixp) for ixp in ixps)

def print_statistics (ixps):
    
    global_asns4 = collections.Counter()
    global_asns6 = collections.Counter()
    memberships4 = set()
    memberships6 = set()
    
    for ixp in ixps:
        if ixp.members4[0]:
            asns4 = collections.Counter(normalize_asns(get_column ("ASN", *ixp.members4)))
            global_asns4.update (asns4)
            memberships4.update ( (ixp.id, asn) for asn in asns4 )
        else:
            asns4 = collections.Counter()
            
        if ixp.members6[0]:
            asns6 = collections.Counter(normalize_asns(get_column ("ASN", *ixp.members6)))
            global_asns6.update (asns6)
            memberships6.update ( (ixp.id, asn) for asn in asns6 )
        else:
            asns6 = collections.Counter()

        all_asns = asns4 + asns6
        pch_edges = set (pch_ixp_edges.get (ixp.id, []))
        shortname = ixp.metadata["Short Name"]

        print ixp.id, len(asns4), len(asns6), len(all_asns), len (pch_edges), len(pch_edges.intersection(all_asns)), shortname
    
    
    print >>sys.stderr, len(global_asns4), len(global_asns6), len(memberships4), len(memberships6)


def iter_data (ixps, version, *colnames):
    for ixp in ixps:
        if version == 4:
            members = ixp.members4
        elif version == 6:
            members = ixp.members6
        else:
            raise ValueError ("No such version: %s" % version)

        # if we don't have a header, then the dataframe is empty => return empty list
        if members[0]:
            yield get_columns (colnames, *members)
        else:
            yield []

def iter_data_nonempty (ixps, version, *colnames):
    empty = u'' if len(colnames) == 1 else (u'',)*len(colnames)

    for data in iter_data (ixps, version, *colnames):
        yield [ item for item in data if item != empty ]


def make_statistics (ixps, version, *colnames):
    return [ collections.Counter (v) for v in iter_data (ixps, version, *colnames) ]

def _count_unique (nonempty, ixps, versions, *colnames):
    if nonempty:
        iterator = iter_data_nonempty
    else:
        iterator = iter_data

    if type(versions) == int:
        versions = [ versions ]
    return [ len (set (v)) for v in itertools.chain (
        *(iterator (ixps, version, *colnames) for version in versions) ) ]

def count_unique (ixps, versions, *colnames):
    return _count_unique (False, ixps, versions, *colnames)
        
def count_unique_nonempty (ixps, versions, *colnames):
    return _count_unique (True, ixps, versions, *colnames)

def search_ixp (ixps, substr):
    """A helper to quickly spot the right IXP id in interactive use"""
    for ixp in ixps:
        if substr in ixp.longname:
            print ixp.id, len(ixp.members4[1]), len(ixp.members6[1]), ixp.longname

def check_name_ambiguity(ixps, versions=(4, 6)):
    a_to_o = collections.defaultdict(set)
    o_to_a = collections.defaultdict(set)
    
    def add_data (version):
        for asn, orga in itertools.chain (*iter_data(ixps, version, "ASN", "Organization")):
            a_to_o[asn].add(orga)
            o_to_a[orga].add(asn)
    
    for version in versions:
        add_data(version)
    
    ambi_ao = [ ao for ao in a_to_o.iteritems() if len(ao[1]) != 1 ]
    ambi_oa = [ oa for oa in o_to_a.iteritems() if len(oa[1]) != 1 ]
    
    print len(ambi_ao), ambi_ao
    print len(ambi_oa), ambi_oa

def membership_summary (ixps, versions, *columns):
    return sum (count_unique_nonempty (ixps, versions, *columns))

def membership_summaries (ixps, *columns):
    return tuple (membership_summary (ixps, v, *columns)
                  for v in (4, 6, (4,6))
                  )

def format_membership_summary (ixps, *columns):
    f = "%21s:  v4 = %5d   v6 = %5d   all = %5d"
    summaries = membership_summaries (ixps, *columns)
    return f % (("+".join(columns),) + summaries)
        
def format_membership_summary_4 (ixps, *columns):
    f = "%21s:  v4 = %5d"
    summary = membership_summary (ixps, 4, *columns)
    return f % ("+".join(columns), summary)

def get_slash24 (ip):
    return ip.rsplit(".", 1)[0] + ".0"

def _check_double_reporting (ixps, version):
    if type(ixps) != list:
        ixps = list(ixps)
    duplicate_indexes = [ i for i, x in enumerate (iter_data (ixps, version, "IP")) if len(set(x)) != len(x) ]
    for i in duplicate_indexes:
        ixp = ixps[i]
        members = ixp.members4 if version == 4 else ixp.members6
        IPs = get_column ("IP", *members)
        cIP = collections.Counter (IPs)
        ip_stats = collections.Counter (cIP.itervalues())
        multiples = dict ((ip, c) for ip, c in cIP.iteritems() if c > 1)
        slash24_stats = collections.Counter (get_slash24(ip) for ip in multiples)
        yield ixp.id, ip_stats, slash24_stats, ixp.longname, multiples

def print_double_reporting (report, version):
    if report:
        print "Version", version, "double reporting:"
    for stats in report:
        print " ".join (str(i) for i in stats[:3])
    

def check_double_reporting (ixps, do_print=False):
    reports = dict()
    for version in (4, 6):
        reports[version] = list (_check_double_reporting (ixps, version))
        if do_print:
            print_double_reporting (reports[version], version)
    return reports
    
def print_nonidentical_stats4 (ixp_dict, report4):
    dupl_data = collections.defaultdict(set)
    for id_, ip_stats, slash24_stats, longname, multiples in report4:
        ixp = ixp_dict[id_]
        ips = set(multiples.keys())
        colnames = (u'IP', u'FQDN', u'Ping', u'BGP', u'ASN', u'Organization', u'Prefixes', u'TXT Records')
        dataframe = get_columns (colnames, *ixp.members4)
        for row in dataframe:
            ip = row[0]
            data = tuple(row[1:])
            dupl_data[ip].add(data)

        for ip, data in dupl_data.iteritems():
            if len(data) > 1:
                print "Non-identical duplicates for", ip, data

if __name__ == "__main__":
    if not "ixp_dict" in dir():
        ixp_dict = read_data()
    ixps = sorted (ixp_dict.itervalues(), key=lambda ixp: int(ixp.id))
    
    print "Ambiguous mappings from ASN to Organization name"
    check_name_ambiguity (ixps)
    print
    
    print "#Memberships by ('IP' is the upper bound)"
    for combinations in ("IP", "ASN", "Organization", "ASN+Organization"):
        print format_membership_summary (ixps, *combinations.split("+"))
    
    for combinations in ("FQDN", "FQDN+ASN", "FQDN+Organization",
                         "FQDN+ASN+Organization", "TXT Records"):
        print format_membership_summary_4 (ixps,  *combinations.split("+"))    

    print
    double_reporting = check_double_reporting (ixps, True)
    print_nonidentical_stats4 (ixp_dict, double_reporting[4])
    if len (double_reporting[6]) > 0:
        print "Double reporting for version 6 detected. Please check manually for non-identical duplicates."
